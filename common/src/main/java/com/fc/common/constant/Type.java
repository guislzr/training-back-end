package com.fc.common.constant;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/6 14:08
 */
public class Type {
    public static final String[] TYPE_LIST = {"家居日用","节日用品","儿童用品","文化娱乐","办公用品","精美服饰","鞋类箱包",
            "珠宝配饰","护肤彩妆","家居装饰","交通工具","家用电器","数码设备","户外用品","其他"};
}
