package com.fc.common.convert;

import com.fc.common.domain.AlbumDo;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.vo.AlbumVo;
import com.fc.common.vo.CommodityCartVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:46
 */
@Mapper(componentModel = "spring")
public interface AlbumDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    AlbumDo convert(AlbumVo albumVo);

    AlbumVo convert(AlbumDo albumDo);
}
