package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.UserDo;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.UserPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:48
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface UserDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    UserPo convert(UserDo userDo);


    UserDo convert(UserPo userPo);
}
