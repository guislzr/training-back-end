package com.fc.common.convert;

import com.fc.common.domain.ReportDo;
import com.fc.common.vo.ReportVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/21 16:40
 */
@Mapper(componentModel = "spring")
public interface ReportDomainConvert {

//    @Mappings({@Mapping(target = "id", ignore = true)})
    ReportDo convert(ReportVo reportVo);

    ReportVo convert(ReportDo reportDo);
}
