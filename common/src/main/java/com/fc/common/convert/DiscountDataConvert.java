package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.DiscountDo;
import com.fc.common.po.DiscountPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 10:07
 */
@Mapper(componentModel = "spring")
public interface DiscountDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    DiscountPo convert(DiscountDo discountDo);

    DiscountDo convert(DiscountPo discountPo);
}
