package com.fc.common.convert;

import com.fc.common.domain.UserDo;
import com.fc.common.vo.UserCreateVo;
import com.fc.common.vo.UserVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:48
 */
@Mapper(componentModel = "spring")
public interface UserDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    UserDo convert(UserVo userVo);

//    @Mappings({@Mapping(target = "id", ignore = true)})
    UserDo convert(UserCreateVo userCreateVo);

    UserVo convert(UserDo userDo);
}
