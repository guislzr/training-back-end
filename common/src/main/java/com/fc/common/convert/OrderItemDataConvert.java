package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.OrderStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 12:33
 */
@Mapper(componentModel = "spring", imports={JSONObject.class, Map.class, List.class, TypeReference.class, OrderStatus.class})
public interface OrderItemDataConvert {
    @Mappings({
            @Mapping(target = "uniform", expression = "java(JSONObject.toJSONString(orderItemDo.getUniform()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", expression = "java(orderItemDo.getStatus().getNum())"),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    OrderItemPo convert(OrderItemDo orderItemDo);

    @Mappings({
            @Mapping(target = "status", expression = "java(OrderStatus.toStatus(orderItemPo.getStatus()))"),
            @Mapping(target = "uniform", expression = "java(JSONObject.parseObject(orderItemPo.getUniform(), new TypeReference<Map<String,String>>(){}))"),
    })
    OrderItemDo convert(OrderItemPo orderItemPo);
}
