package com.fc.common.convert;

import com.fc.common.domain.ProblemAfterSaleDo;
import com.fc.common.vo.ProblemCreateVo;
import com.fc.common.vo.ProblemShowVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 15:22
 */
@Mapper(componentModel = "spring")
public interface ProblemDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    ProblemAfterSaleDo convert(ProblemCreateVo problemCreateVo);

    ProblemShowVo convert(ProblemAfterSaleDo problemAfterSaleDo);
}
