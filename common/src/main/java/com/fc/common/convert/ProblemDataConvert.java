package com.fc.common.convert;

import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.ProblemAfterSaleDo;
import com.fc.common.po.EvaluationPo;
import com.fc.common.po.ProblemAfterSalePo;
import com.fc.common.po.status.OrderStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 15:22
 */
@Mapper(componentModel = "spring")
public interface ProblemDataConvert {
    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    ProblemAfterSalePo convert(ProblemAfterSaleDo problemAfterSaleDo);

    @Mappings({
            @Mapping(target = "createTime", source = "problemAfterSalePo.gmtCreate")
    })
    ProblemAfterSaleDo convert(ProblemAfterSalePo problemAfterSalePo);
}
