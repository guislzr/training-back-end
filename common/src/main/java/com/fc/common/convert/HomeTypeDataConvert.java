package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.HomeTypeDo;
import com.fc.common.po.HomeTypePo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:27
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface HomeTypeDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    HomeTypePo convert(HomeTypeDo homeTypeListDo);


    HomeTypeDo convert(HomeTypePo homeTypeListPo);
}
