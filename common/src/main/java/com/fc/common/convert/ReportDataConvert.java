package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.ReportDo;
import com.fc.common.po.ReportPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/21 15:35
 */
@Mapper(componentModel = "spring", imports = {JSONObject.class})
public interface ReportDataConvert {

    @Mappings({
            @Mapping(target = "labels", expression = "java(JSONObject.toJSONString(reportDo.getLabels()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    ReportPo convert(ReportDo reportDo);

    @Mappings({
            @Mapping(target = "labels", expression = "java(JSONObject.parseArray(reportPo.getLabels(), String.class))")
    })
    ReportDo convert(ReportPo reportPo);
}
