package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.StoreDo;
import com.fc.common.po.StorePo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.StoreVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:46
 */
@Mapper(componentModel = "spring")
public interface StoreDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    StoreDo convert(StoreVo storeVo);


    StoreVo convert(StoreDo storeDo);
}
