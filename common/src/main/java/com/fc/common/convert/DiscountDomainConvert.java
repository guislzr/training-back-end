package com.fc.common.convert;

import com.fc.common.domain.DiscountDo;
import com.fc.common.vo.DiscountVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 10:18
 */
@Mapper(componentModel = "spring")
public interface DiscountDomainConvert {

//    @Mappings({@Mapping(target = "id", ignore = true)})
    DiscountDo convert(DiscountVo discountVo);

    DiscountVo convert(DiscountDo discountDo);
}
