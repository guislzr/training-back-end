package com.fc.common.convert;

import com.fc.common.domain.OrderDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.vo.OrderCreateVo;
import com.fc.common.vo.OrderItemCreateVo;
import com.fc.common.vo.OrderItemShowVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 12:34
 */
@Mapper(componentModel = "spring")
public interface OrderItemDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    OrderItemDo convert(OrderItemCreateVo orderItemCreateVo);

    OrderItemShowVo convert(OrderItemDo orderItemDo);
}
