package com.fc.common.convert;

import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.FreightTemplateDo;
import com.fc.common.po.EvaluationPo;
import com.fc.common.po.FreightTemplatePo;
import com.fc.common.po.status.ChargeMode;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:02
 */
@Mapper(componentModel = "spring", imports = {ChargeMode.class})
public interface FreightTemplateDataConvert {
    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "chargeMode", expression = "java(freightTemplateDo.getChargeMode().getNum())"),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    FreightTemplatePo convert(FreightTemplateDo freightTemplateDo);

    @Mappings({
            @Mapping(target = "createTime", source = "freightTemplatePo.gmtCreate"),
            @Mapping(target = "chargeMode", expression = "java(ChargeMode.toMode(freightTemplatePo.getChargeMode()))")
    })
    FreightTemplateDo convert(FreightTemplatePo freightTemplatePo);
}
