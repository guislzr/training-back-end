package com.fc.common.convert;

import com.fc.common.domain.NoticeDo;
import com.fc.common.po.NoticePo;
import com.fc.common.po.status.NoticeStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 15:58
 */
@Mapper(componentModel = "spring", imports = {NoticeStatus.class})
public interface NoticeDataConvert {
    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", expression = "java(noticeDo.getStatus().getNum())"),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    NoticePo convert(NoticeDo noticeDo);

    @Mappings({
            @Mapping(target = "status", expression = "java(NoticeStatus.toStatus(noticePo.getStatus()))"),
            @Mapping(target = "createTime", source = "gmtCreate")
    })
    NoticeDo convert(NoticePo noticePo);
}
