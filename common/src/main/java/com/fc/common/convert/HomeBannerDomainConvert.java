package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.HomeBannerDo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.HomeBannerVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:26
 */
@Mapper(componentModel = "spring")
public interface HomeBannerDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    HomeBannerDo convert(HomeBannerVo homeBannerVo);

    HomeBannerVo convert(HomeBannerDo homeBannerDo);
}
