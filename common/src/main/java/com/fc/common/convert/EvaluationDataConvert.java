package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fc.common.domain.EvaluationDo;
import com.fc.common.po.EvaluationPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 22:08
 */
@Mapper(componentModel = "spring",imports={JSONObject.class})
public interface EvaluationDataConvert {
    @Mappings({
            @Mapping(target = "pictures", expression = "java(JSONObject.toJSONString(evaluationDo.getPictures()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    EvaluationPo convert(EvaluationDo evaluationDo);

    @Mappings({

            @Mapping(target = "time", source = "gmtCreate"),
            @Mapping(target = "pictures", expression = "java(JSONObject.parseArray(evaluationPo.getPictures(), String.class))")
    })
    EvaluationDo convert(EvaluationPo evaluationPo);
}
