package com.fc.common.convert;

import com.fc.common.domain.OrderDo;
import com.fc.common.vo.OrderCreateVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:23
 */
@Mapper(componentModel = "spring")
public interface OrderDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    OrderDo convert(OrderCreateVo orderCreateVo);

    OrderCreateVo convert(OrderDo orderDo);
}
