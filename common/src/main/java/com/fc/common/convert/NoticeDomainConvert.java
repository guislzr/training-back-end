package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.NoticeDo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.NoticeVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 16:03
 */
@Mapper(componentModel = "spring")
public interface NoticeDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    NoticeDo convert(NoticeVo noticeVo);

    NoticeVo convert(NoticeDo noticeDo);
}
