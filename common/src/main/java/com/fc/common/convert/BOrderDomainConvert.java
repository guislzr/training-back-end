package com.fc.common.convert;

import com.fc.common.domain.OrderDo;
import com.fc.common.vo.BOrderShowVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/5 21:27
 */
@Mapper(componentModel = "spring")
public interface BOrderDomainConvert {
    BOrderShowVo convert(OrderDo orderDo);
}
