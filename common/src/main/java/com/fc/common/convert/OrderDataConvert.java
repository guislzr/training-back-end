package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.OrderDo;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.OrderStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:56
 */
@Mapper(componentModel = "spring", imports={JSONObject.class, OrderStatus.class})
public interface OrderDataConvert {

    @Mappings({
            @Mapping(target = "orderItemIds", expression = "java(JSONObject.toJSONString(orderDo.getOrderItemIds()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", expression = "java(orderDo.getStatus().getNum())"),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    OrderPo convert(OrderDo orderDo);

    @Mappings({
            @Mapping(target = "status", expression = "java(OrderStatus.toStatus(orderPo.getStatus()))"),
            @Mapping(target = "orderItemIds", expression = "java(JSONObject.parseArray(orderPo.getOrderItemIds(), Long.class))"),
    })
    OrderDo convert(OrderPo orderPo);
}
