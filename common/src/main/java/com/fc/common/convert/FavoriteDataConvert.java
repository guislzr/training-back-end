package com.fc.common.convert;


import com.baomidou.mybatisplus.annotation.TableId;
import com.fc.common.domain.FavoriteDo;
import com.fc.common.po.FavoritePo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 12:57
 */
@Mapper(componentModel = "spring")
public interface FavoriteDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    FavoritePo convert(FavoriteDo favoriteDo);

    FavoriteDo convert(FavoritePo favoritePo);
}
