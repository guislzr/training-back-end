package com.fc.common.convert;

import com.fc.common.domain.AddressDo;
import com.fc.common.po.AddressPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:50
 */
@Mapper(componentModel = "spring")
public interface AddressDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    AddressPo convert(AddressDo addressDo);

    AddressDo convert(AddressPo addressPo);
}
