package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.StoreDo;
import com.fc.common.po.StorePo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:11
 */
@Mapper(componentModel = "spring")
public interface StoreDataConvert {
    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    StorePo convert(StoreDo storeDo);


    StoreDo convert(StorePo storePo);
}
