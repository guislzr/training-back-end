package com.fc.common.convert;

import com.fc.common.domain.AdminDo;
import com.fc.common.domain.NoticeDo;
import com.fc.common.vo.AdminVo;
import com.fc.common.vo.NoticeVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 18:19
 */
@Mapper(componentModel = "spring")
public interface AdminDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    AdminDo convert(AdminVo adminVo);

    AdminVo convert(AdminDo adminDo);
}
