package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.MessageDo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.MessageVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:54
 */
@Mapper(componentModel = "spring")
public interface MessageDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    MessageDo convert(MessageVo messageVo);

    MessageVo convert(MessageDo messageDo);
}
