package com.fc.common.convert;

import com.fc.common.domain.CommodityCartDo;
import com.fc.common.domain.CommodityDo;
import com.fc.common.vo.CommodityCartVo;
import com.fc.common.vo.CommodityVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 11:58
 */
@Mapper(componentModel = "spring")
public interface CommodityCartDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    CommodityCartDo convert(CommodityCartVo commodityCartVo);

    CommodityCartVo convert(CommodityCartDo commodityCartDo);
}
