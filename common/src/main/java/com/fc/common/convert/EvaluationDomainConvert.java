package com.fc.common.convert;

import com.fc.common.domain.EvaluationDo;
import com.fc.common.vo.EvaluationVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 22:41
 */
@Mapper(componentModel = "spring")
public interface EvaluationDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    EvaluationDo convert(EvaluationVo evaluationVo);


    EvaluationVo convert(EvaluationDo evaluationDo);
}
