package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.MessageDo;
import com.fc.common.po.MessagePo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:54
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface MessageDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    MessagePo convert(MessageDo messageDo);


    MessageDo convert(MessagePo messagePo);
}
