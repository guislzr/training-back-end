package com.fc.common.convert;

import com.fc.common.domain.AddressDo;
import com.fc.common.vo.AddressVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:54
 */
@Mapper(componentModel = "spring")
public interface AddressDomainConvert {

//    @Mappings({@Mapping(target = "id", ignore = true)})
    AddressDo convert(AddressVo addressVo);

    AddressVo convert(AddressDo addressDo);
}
