package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.WalletDo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.WalletVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:22
 */
@Mapper(componentModel = "spring")
public interface WalletDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    WalletDo convert(WalletVo walletVo);

    WalletVo convert(WalletDo walletDo);
}
