package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.domain.CommodityDo;
import com.fc.common.po.CommodityCartPo;
import com.fc.common.po.CommodityPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:02
 */
@Mapper(componentModel = "spring", imports={JSONObject.class, Map.class, List.class, TypeReference.class})
public interface CommodityCartDataConvert {
    @Mappings({
            @Mapping(target = "commodityIds", expression = "java(JSONObject.toJSONString(commodityCartDo.getCommodityIds()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    CommodityCartPo convert(CommodityCartDo commodityCartDo);

    @Mappings({
            @Mapping(target = "commodityIds", expression = "java(JSONObject.parseObject(commodityCartPo.getCommodityIds(), new TypeReference<Map<Long, Long>>(){}))"),
    })
    CommodityCartDo convert(CommodityCartPo commodityCartPo);
}
