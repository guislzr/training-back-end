package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fc.common.domain.FavoriteStoreDo;
import com.fc.common.po.FavoriteStorePo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;
import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:08
 */
@Mapper(componentModel = "spring")
public interface FavoriteStoreDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    FavoriteStorePo convert(FavoriteStoreDo favoriteStoreDo);

    FavoriteStoreDo convert(FavoriteStorePo favoriteStorePo);
}
