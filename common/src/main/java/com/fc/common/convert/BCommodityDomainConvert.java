package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.vo.BCommodityVo;
import com.fc.common.vo.CommodityVo;
import org.mapstruct.Mapper;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/5 20:40
 */
@Mapper(componentModel = "spring")
public interface BCommodityDomainConvert {
    BCommodityVo convert(CommodityDo commodityDo);
}
