package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.AlbumDo;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.po.AlbumPo;
import com.fc.common.po.CommodityCartPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:46
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface AlbumDataConvert {
    @Mappings({
            @Mapping(target = "pictures", expression = "java(JSONObject.toJSONString(albumDo.getPictures()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    AlbumPo convert(AlbumDo albumDo);

    @Mappings({
            @Mapping(target = "pictures", expression = "java(JSONObject.parseArray(albumPo.getPictures(), String.class))"),
    })
    AlbumDo convert(AlbumPo albumPo);
}
