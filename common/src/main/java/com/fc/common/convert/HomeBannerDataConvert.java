package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.HomeBannerDo;
import com.fc.common.po.HomeBannerPo;
import com.fc.common.po.CommodityPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:20
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface HomeBannerDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    HomeBannerPo convert(HomeBannerDo homeBannerDo);


    HomeBannerDo convert(HomeBannerPo homeBannerPo);
}
