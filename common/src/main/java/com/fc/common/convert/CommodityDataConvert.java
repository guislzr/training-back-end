package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.fc.common.domain.CommodityDo;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.status.CommodityStatus;
import com.fc.common.po.status.RentTimeStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.*;

/**
 * @author lzr
 * @date 2021/3/18 14:43
 */

@Mapper(componentModel = "spring", imports={JSONObject.class, Map.class, List.class, TypeReference.class, CommodityStatus.class, RentTimeStatus.class})
public interface CommodityDataConvert {

    @Mappings({
            @Mapping(target = "uniform", expression = "java(JSONObject.toJSONString(commodityDo.getUniform()))"),
            @Mapping(target = "attribute", expression = "java(JSONObject.toJSONString(commodityDo.getAttribute()))"),
            @Mapping(target = "type", expression = "java(JSONObject.toJSONString(commodityDo.getType()))"),
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "rentTime", expression = "java(commodityDo.getRentTime().getNum())"),
            @Mapping(target = "commodityStatus", expression = "java(commodityDo.getCommodityStatus().getNum())"),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    CommodityPo convert(CommodityDo commodityDo);

    @Mappings({
            @Mapping(target = "uniform", expression = "java(JSONObject.parseObject(commodityPo.getUniform(), new TypeReference<Map<String, List<String>>>(){}))"),
            @Mapping(target = "attribute", expression = "java(JSONObject.parseObject(commodityPo.getAttribute(), new TypeReference<Map<String, String>>(){}))"),
            @Mapping(target = "type", expression = "java(JSONObject.parseArray(commodityPo.getType(), String.class))"),
            @Mapping(target = "rentTime", expression = "java(RentTimeStatus.toStatus(commodityPo.getRentTime()))"),
            @Mapping(target = "commodityStatus", expression = "java(CommodityStatus.toStatus(commodityPo.getCommodityStatus()))")
    })
    CommodityDo convert(CommodityPo commodityPo);

}
