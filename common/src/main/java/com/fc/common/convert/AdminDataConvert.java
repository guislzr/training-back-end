package com.fc.common.convert;

import com.fc.common.domain.AdminDo;
import com.fc.common.po.AdminPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 18:18
 */
@Mapper(componentModel = "spring")
public interface AdminDataConvert {
    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    AdminPo convert(AdminDo adminDo);

    AdminDo convert(AdminPo adminPo);
}
