package com.fc.common.convert;

import com.alibaba.fastjson.JSONObject;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.WalletDo;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.WalletPo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:21
 */
@Mapper(componentModel = "spring", imports={JSONObject.class})
public interface WalletDataConvert {

    @Mappings({
//            @Mapping(target = "id", ignore = true),
            @Mapping(target = "gmtCreate", ignore = true),
            @Mapping(target = "gmtUpdate", ignore = true)
    })
    WalletPo convert(WalletDo walletDo);


    WalletDo convert(WalletPo walletPo);
}
