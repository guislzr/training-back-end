package com.fc.common.convert;

import com.fc.common.domain.HomeTypeDo;
import com.fc.common.vo.HomeTypeVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:28
 */
@Mapper(componentModel = "spring")
public interface HomeTypeDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    HomeTypeDo convert(HomeTypeVo homeTypeVo);

    HomeTypeVo convert(HomeTypeDo homeTypeDo);
}
