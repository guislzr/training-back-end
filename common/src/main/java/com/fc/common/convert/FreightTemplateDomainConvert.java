package com.fc.common.convert;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.FreightTemplateDo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.FreightTemplateVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:05
 */
@Mapper(componentModel = "spring")
public interface FreightTemplateDomainConvert {
//    @Mappings({@Mapping(target = "id", ignore = true)})
    FreightTemplateDo convert(FreightTemplateVo freightTemplateVo);

    FreightTemplateVo convert(FreightTemplateDo freightTemplateDo);
}
