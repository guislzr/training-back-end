package com.fc.common.convert;

import com.fc.common.domain.FavoriteDo;
import com.fc.common.vo.FavoriteVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 13:04
 */
@Mapper(componentModel = "spring")
public interface FavoriteDomainConvert {

//    @Mappings({@Mapping(target = "id", ignore = true)})
    FavoriteDo convert(FavoriteVo favoriteVo);

    FavoriteVo convert(FavoriteDo favoriteDo);
}
