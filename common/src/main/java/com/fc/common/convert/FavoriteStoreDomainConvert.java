package com.fc.common.convert;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fc.common.domain.FavoriteStoreDo;
import com.fc.common.vo.FavoriteStoreVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:08
 */
@Mapper(componentModel = "spring")
public interface FavoriteStoreDomainConvert {

//    @Mappings({@Mapping(target = "id", ignore = true)})
    FavoriteStoreDo convert(FavoriteStoreVo favoriteStoreVo);

    FavoriteStoreVo convert(FavoriteStoreDo favoriteStoreDo);
}
