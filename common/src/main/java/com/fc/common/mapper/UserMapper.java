package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.UserPo;
import org.apache.ibatis.annotations.Options;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:46
 */
public interface UserMapper extends BaseMapper<UserPo> {
    @Override
    @Options(useGeneratedKeys = true,keyProperty ="id",keyColumn = "id")
    int insert(UserPo userPo);


}
