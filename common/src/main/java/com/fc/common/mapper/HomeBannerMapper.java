package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.HomeBannerPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:17
 */
@Mapper
public interface HomeBannerMapper extends BaseMapper<HomeBannerPo> {
}
