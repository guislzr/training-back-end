package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.AlbumPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:57
 */
@Mapper
public interface AlbumMapper extends BaseMapper<AlbumPo> {
}
