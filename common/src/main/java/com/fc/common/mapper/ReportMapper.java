package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.ReportPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther: 李梓睿
 * @date: 2021/3/20 11:35
 */
@Mapper
public interface ReportMapper extends BaseMapper<ReportPo> {

}
