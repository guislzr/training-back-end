package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.NoticePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:19
 */
@Mapper
public interface NoticeMapper extends BaseMapper<NoticePo> {
}
