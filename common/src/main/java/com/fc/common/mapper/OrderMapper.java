package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.OrderPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import org.apache.ibatis.annotations.Options;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:52
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderPo> {

    @Select("SELECT COUNT(*) FROM order_list WHERE DATE_FORMAT( endTime, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) AND storeId=#{storeId}")
    Integer queryMonthOrderNumber(Long storeId);

    @Select("SELECT SUM(payment) FROM order_list WHERE DATE_FORMAT( endTime, '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' ) AND storeId=#{storeId}")
    Double queryMonthOrderAmount(Long storeId);

    @Select("SELECT SUM(payment) FROM order_list WHERE TO_DAYS(paymentTime) = TO_DAYS(now()) AND storeId=#{storeId}")
    Double queryTodayOrderAmount(Long storeId);

    @Select("SELECT COUNT(*) FROM order_list WHERE TO_DAYS( NOW( ) ) - TO_DAYS( create_time) = #{differ} AND storeId = #{storeId}")
    Integer queryOrderNumber(@Param("storeId") Long storeId, @Param("differ")Integer differ);

    @Select("SELECT SUM(payment) FROM order_list WHERE TO_DAYS( NOW( ) ) - TO_DAYS( create_time) = 1 AND storeId=#{storeId}")
    Double queryYesterdayOrderAmount(Long storeId);

    @Override
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insert(OrderPo orderPo);
}
