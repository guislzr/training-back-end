package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.FreightTemplatePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:15
 */
@Mapper
public interface FreightTemplateMapper extends BaseMapper<FreightTemplatePo> {
}
