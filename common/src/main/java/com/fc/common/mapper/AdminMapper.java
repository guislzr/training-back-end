package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.domain.AdminDo;
import com.fc.common.po.AdminPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 17:15
 */
@Mapper
public interface AdminMapper extends BaseMapper<AdminPo> {
    @Select("select admin.Id,admin.storeId,admin.adminId,nickname,icon,phone,email from user,admin where admin.storeId=#{storeId} and admin.adminId=user.Id")
    public List<AdminDo> selectAdminByStore(Long storeId);
}
