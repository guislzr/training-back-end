package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.AddressPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:37
 */
@Mapper
public interface AddressMapper extends BaseMapper<AddressPo> {
}
