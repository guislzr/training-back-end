package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.FavoriteStorePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:09
 */
@Mapper
public interface FavoriteStoreMapper extends BaseMapper<FavoriteStorePo> {
}
