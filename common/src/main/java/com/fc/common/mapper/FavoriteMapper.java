package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.FavoritePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 12:19
 */
@Mapper
public interface FavoriteMapper extends BaseMapper<FavoritePo> {
}
