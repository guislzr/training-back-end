package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.MessagePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 11:32
 */
@Mapper
public interface MessageMapper extends BaseMapper<MessagePo> {
}
