package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.CommodityCartPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:09
 */
@Mapper
public interface CommodityCartMapper extends BaseMapper<CommodityCartPo> {
}
