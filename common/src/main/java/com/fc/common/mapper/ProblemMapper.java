package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.ProblemAfterSalePo;
import com.fc.common.po.StorePo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 23:22
 */
@Mapper
public interface ProblemMapper extends BaseMapper<ProblemAfterSalePo> {
}
