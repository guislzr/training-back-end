package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.SearchRecordPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lzr
 * @date 2021/7/10 14:58
 */
@Mapper
public interface SearchRecordMapper extends BaseMapper<SearchRecordPo> {
}
