package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.OrderPo;
import com.fc.common.po.WalletPo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:20
 */
public interface WalletMapper extends BaseMapper<WalletPo> {
}
