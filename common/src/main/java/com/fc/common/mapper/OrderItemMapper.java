package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.domain.HeatCommodity;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.OrderPo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 12:35
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItemPo> {
    @Override
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insert(OrderItemPo orderItemPo);

    /**
     * 查询热度前五的商品
     * @param storeId 商店id
     * @return k:商品id ， v:购买量
     */
    @Select("SELECT commodityId,COUNT(*) as count FROM order_item WHERE storeId=#{storeId} GROUP BY commodityId ORDER BY count DESC LIMIT 5")
    List<HeatCommodity> selectHeatCommodity(Long storeId);
}
