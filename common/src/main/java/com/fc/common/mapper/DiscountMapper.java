package com.fc.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fc.common.po.DiscountPo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @auther: 李梓睿
 * @date: 2021/3/22 22:44
 */
@Mapper
public interface DiscountMapper extends BaseMapper<DiscountPo> {
}
