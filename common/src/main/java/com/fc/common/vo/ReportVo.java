package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/20 11:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportVo {

    private Long id;

    private Long userId;

    private Long storeId;

    private Long commodityId;

    private List<String> labels;

    private String content;
    //todo 照片
}
