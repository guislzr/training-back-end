package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 9:57
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 某个商品的Vo列表
 */
public class EvaluationVoList {
    /**
     * 商品id
     */
    private Long commodityId;

    private List<EvaluationVo> evaluationVoList;

}
