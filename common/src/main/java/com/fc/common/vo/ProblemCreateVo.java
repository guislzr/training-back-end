package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 22:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProblemCreateVo {
    private Long id;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 店铺
     */
    private Long storeId;

    /**
     * 问题描述
     */
    private String problemDescription;

    /**
     * 问题是否解决
     */
    private Boolean isSolve;
}
