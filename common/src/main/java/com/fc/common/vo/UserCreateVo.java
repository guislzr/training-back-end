package com.fc.common.vo;

import com.fc.common.annotation.Phone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/9 18:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateVo {

    @NotBlank(message = "手机号")
    @Phone
    private String phone;

    @NotBlank(message = "密码不能为空")
    @Size(min = 6,max = 18, message = "密码最短为6位，最长为18位")
    private String password;

}
