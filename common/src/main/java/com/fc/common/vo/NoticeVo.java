package com.fc.common.vo;

import com.fc.common.po.status.NoticeStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeVo {
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 推送标题
     */
    private String title;

    /**
     * 推送内容
     */
    private String content;

    /**
     * 阅读量
     */
    private Long readNumber;

    /**
     * 推送量
     */
    private Long pushNumber;

    /**
     * 是否隐藏（状态）
     */
    private NoticeStatus status;

    /**
     * 发布时间
     */
    private Date createTime;
}
