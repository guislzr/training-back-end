package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @auther: 李梓睿
 * @date: 2021/3/22 22:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiscountVo {
    private Long id;

    private Long userId;

    private Long storeId;

    /**
     * 可使用起止日期
     */
    private Date startDate;

    /**
     * 使用截至日期
     */
    private Date expiryDate;

    /**
     * 优惠券使用的门槛值
     */
    private Double thresholdPrice;

    /**
     * 优惠金额
     * eg. 满200减40 thresholdPrice = 200; discountPrice = 40
     */
    private Double discountPrice;

}
