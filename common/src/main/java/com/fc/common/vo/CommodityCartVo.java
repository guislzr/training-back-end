package com.fc.common.vo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/25 15:34
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 购物车显示
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommodityCartVo {
    private Long id;

    /**
     * 商品id
     * k--商品id
     * v--数量
     * Map<Long,Long>
     */
    private Map<Long,Long> commodityIds;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 收货地址
     */
    private Long receiverAddressId;
}
