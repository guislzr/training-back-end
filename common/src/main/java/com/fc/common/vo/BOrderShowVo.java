package com.fc.common.vo;

import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/28 20:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BOrderShowVo {
    private Long id;

    /**
     * 支付时间(下单时间)
     */
    private Date paymentTime;

    /**
     * 商品信息
     * k:商品名称
     * v:购买数量
     */
    private List<OrderItemShowVo> orderItems;

    /**
     * 商品押金
     */
    private Double guaranteePrice;

    /**
     * 总价（实付款）
     */
    private Double payment;

    /**
     * 运费
     */
    private Double transportPrice;

    /**
     * 用户（买家id）
     */
    private Long userId;

    /**
     * 买家昵称
     */
    private String userNickname;

    /**
     * 配送方式
     */
    private String deliveryMode;

    /**
     * 订单状态
     */
    private OrderStatus status;
}
