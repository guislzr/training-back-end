package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationVo {
    /**
     * id
     */
    private Long id;

    private Long userId;

    /**
     * 评价人昵称
     */
    private String userNickname;

    /**
     * 商品名称
     */
    private String commodityName;

    private Long commodityId;

    /**
     * 评价时间
     */
    private Date time;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 回复
     */
    private String reply;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 图片
     */
    private List<String> pictures;

}
