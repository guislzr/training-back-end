package com.fc.common.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 12:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItemCreateVo implements Serializable {
    private Long id;

    /**
     * 商家
     */
    private Long storeId;


    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 租用天数
     */
    private Integer rentDays;

    /**
     * 商品的自定义属性
     * 例如：
     * 颜色：红白蓝
     * 大小： 1 2 3
     * Map<String,String>
     */
    private Map<String, String> uniform;

    /**
     * 某个订单的一行商品的总价
     */
    private Double payment;

    /**
     * 优惠券
     */
    private Double discount;

    /**
     * 运费
     */
    private Double transportPrice;

    /**
     * 订单状态
     */
    private OrderStatus status;


    /**
     * 支付时间
     */
    private Date paymentTime;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 交易完成时间
     */
    private Date endTime;

    /**
     * 交易关闭时间
     */
    private Date closeTime;



}
