package com.fc.common.vo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:16
 */

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

/**
 * 钱包
 */
public class WalletVo {
    private Long id;

    /**
     * 用户
     */
    private Long userId;

    /**
     * 余额
     */
    private Double balance;
}
