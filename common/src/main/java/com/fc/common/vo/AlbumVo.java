package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumVo {
    /**
     * id
     */
    private Long id;


    /**
     * 商店id
     */
    private String storeId;

    /**
     * 相册名称
     */
    private String name;

    /**
     * 图片
     */
    private List<String> pictures;
}
