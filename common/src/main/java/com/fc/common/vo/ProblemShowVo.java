package com.fc.common.vo;

import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProblemShowVo {
    private Long id;

    /**
     * 订单商品id
     */
    private Long orderItemId;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 顾客昵称
     */
    private String customerNickName;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 总价
     */
    private Double payment;

    /**
     * 订单状态
     */
    private OrderStatus status;

    /**
     * 问题描述
     */
    private String problemDescription;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 问题是否解决
     */
    private Boolean isSolve;
}
