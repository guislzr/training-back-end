package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 首页滚动图
 */
public class HomeBannerVo {
    private Long id;

    /**
     * 滚动图
     * 图片url，店铺id
     */
    private String picture;

    /**
     * 滚动图
     * 图片url，店铺id
     */
    private Long storeId;

}
