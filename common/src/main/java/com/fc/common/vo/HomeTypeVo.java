package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 首页商品分类
 */
public class HomeTypeVo {

    private Long id;

    /**
     * 分类的图标
     */
    private String pType;

    /**
     * 分类的名称
     */
    private String type;

}
