package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 首页商品列表
 */
public class HomeCommodityVo {

    private Long id;

    /**
     * 显示的商品大图
     */
    private String avatar;

    /**
     * 租金
     */
    private Double rentPrice;

    /**
     * 租的天数
     */
    private Integer rentDays;


}
