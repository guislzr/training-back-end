package com.fc.common.vo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/25 15:22
 */

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户收到的消息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageVo {
    private Long id;

    /**
     * 商店id
     */
    private Long storeId;

    /**
     * 发送者id
     */
    private Long senderId;

    /**
     * 接收者id
     */
    private Long receiverId;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String content;



}
