package com.fc.common.vo;

import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/28 15:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * B端首页订单数据vo
 */
public class BOrderStatisticVo {
    /**
     * 订单总数
     */
    private Long totalOrderNumber;

    /**
     * 今日交易金额
     */
    private Double todayTradeAmount;

    /**
     * 本月交易量
     */
    private Integer monthOrderNumber;

    /**
     * 本月交易金额
     */
    private Double monthTradeAmount;

    /**
     * 昨日订单数量
     */
    private Integer yesterdayOrderNumber;

    /**
     * 昨日交易金额
     */
    private Double yesterdayTradeAmount;

    /**
     * 前一周订单数量
     */
    private List<Integer> lastWeekOrderNumbers;

    /**
     * 订单状态分类
     * k：订单状态
     * v：该状态订单数量
     */
    private Map<OrderStatus,Integer> orderOfStatus;
}
