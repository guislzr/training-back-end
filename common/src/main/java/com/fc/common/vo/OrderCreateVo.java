package com.fc.common.vo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/25 15:03
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 创建租赁物品的订单
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderCreateVo implements Serializable {

    private Long id;

    /**
     * 用户
     */
    private Long userId;


    /**
     * 收货地址
     */
    private Long receiverAddressId;

    /**
     * 运费
     */
    private Double transportPrice;

    List<OrderItemCreateVo> orderItemCreateVos;

}
