package com.fc.common.vo;

import com.fc.common.domain.HeatCommodity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/28 16:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * B端首页店铺数据vo
 */
public class BStoreStatisticVo {
    /**
     * 收藏店铺人数
     */
    private Long collectors;

    /**
     * 上架商品数
     */
    private Integer onSaleCommodity;

    /**
     * 待上架商品数
     */
    private Integer waitCommodity;


    /**
     * 销量前五的商品
     */
    private List<HeatCommodity> heatCommodities;
}
