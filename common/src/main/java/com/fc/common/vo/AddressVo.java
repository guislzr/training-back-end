package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressVo {

    private Long id;

    private Long  userId;

    private String receiverName;

    private String receiverPhone;

    private String country;

    private String province;

    private String city;

    private String district;

    private String address;

    private String zip;

    private Boolean isDefault;
}
