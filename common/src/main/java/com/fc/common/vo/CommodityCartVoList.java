package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/25 16:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 购物车列表
 */
public class CommodityCartVoList {

    private List<CommodityCartVo> commodityCartVoList;

    private Double totalGuaranteePrice;
}
