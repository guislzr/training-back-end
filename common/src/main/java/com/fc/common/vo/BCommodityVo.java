package com.fc.common.vo;

import com.fc.common.po.status.CommodityStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/28 15:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BCommodityVo {
    private Long id;

    /**
     * 商品名称
     */
    private String commodityName;

    /**
     * 商品图片
     */
    private String subImages;

    /**
     * 商品所属类别列表
     * 如生活用品
     */
    private List<String> type;

    /**
     * 发布时间
     */
    private Date releaseTime;

    /**
     * 交易量
     */
    private Integer tradeNumber;

    /**
     * 库存
     */
    private Long number;

    /**
     * 商品押金
     */
    private Double guaranteePrice;


    /**
     * 商品状态
     * 在售，下架，待上架
     */
    private CommodityStatus commodityStatus;
}
