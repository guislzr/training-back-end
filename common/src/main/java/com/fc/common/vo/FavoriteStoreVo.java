package com.fc.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteStoreVo {

    private Long id;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "店铺id")
    private Long storeId;

    @ApiModelProperty(value = "店铺名")
    private String storeName;

    /**
     * 订阅时间
     */
    @ApiModelProperty(value = "订阅时间")
    private Date subscribeDate;

    /**
     * 订阅时长
     */
    @ApiModelProperty(value = "订阅时长")
    private Integer subscribeDays;

    /**
     * 店铺头像
     */
    @ApiModelProperty(value = "店铺头像")
    private String storeSubImage;

    /**
     * 店铺评分
     */
    @ApiModelProperty(value = "店铺评分")
    private Double score;

    /**
     * 店铺相关商品的缩略图
     */
    @ApiModelProperty(value = "店铺相关商品缩略图")
    private List<String> subImageLists;
}
