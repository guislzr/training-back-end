package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/21 20:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreVo {

    private Long id;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 店铺头像
     */
    private String icon;

    /**
     * 店铺评分
     */
    private Double score;

    /**
     * 信誉积分
     */
    private Integer creditScore;

    /**
     * 店铺介绍
     */
    private String introduction;
}
