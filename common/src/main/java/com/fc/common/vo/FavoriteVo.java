package com.fc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 12:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteVo {

    Long id;

    /**
     * 商品id
     */
    Long commodityId;

    /**
     * 收藏商品的用户id
     */
    Long userId;

    /**
     * 商品名称
     */
    String commodityName;

    /**
     * 商品图片
     */
    String subImages;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 商品收藏数
     */
    Integer starCount;
}
