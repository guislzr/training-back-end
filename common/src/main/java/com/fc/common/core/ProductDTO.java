package com.fc.common.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lzr
 * @date 2021/7/12 17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

    private Integer productId;

    private String productName;

    private String productPrice;

}
