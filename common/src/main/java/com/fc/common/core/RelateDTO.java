package com.fc.common.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lzr
 * @date 2021/7/12 16:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RelateDTO {
    //用户id
    private Integer userId;
    //业务id
    private Integer productId;
    //指数
    private Integer index;

}
