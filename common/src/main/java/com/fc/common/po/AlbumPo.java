package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 15:38
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("album")
/**
 * 相册
 */
public class AlbumPo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 商店id
     */
    private String storeId;

    /**
     * 相册名称
     */
    private String name;

    /**
     * 图片
     * List<String>
     */
    private String pictures;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
