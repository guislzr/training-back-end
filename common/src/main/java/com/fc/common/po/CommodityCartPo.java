package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/6 16:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_commodity_cart")
/**
 * 购物车
 */
public class CommodityCartPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 商品id
     * k--商品id
     * v--数量
     * Map<Long,Long>
     */
    private String commodityIds;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 收货地址
     */
    private Long receiverAddressId;


    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
