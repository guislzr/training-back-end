package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/7 12:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("b_store")
public class StorePo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 店主id
     */
    private Long storekeeperId;


    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 店铺头像
     */
    private String icon;

    /**
     * 店铺评分
     */
    private Double score;

    /**
     * 信誉积分
     */
    private Integer creditScore;

    /**
     * 店铺介绍
     */
    private String introduction;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
