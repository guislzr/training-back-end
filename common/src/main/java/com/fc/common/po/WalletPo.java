package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Period;
import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("wallet")
public class WalletPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户
     */
    private Long userId;

    /**
     * 余额
     */
    private Double balance;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;

    //TODO 可以加银行卡啥的
}
