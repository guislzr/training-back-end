package com.fc.common.po.status;

import com.fc.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 14:39
 */
@AllArgsConstructor
public enum CommodityStatus {
    /**
     * 在售
     */
    ONSALE(1),
    /**
     * 下架
     */
    SOLDOUT(2),
    /**
     * 待上架
     */
    WAIT(3);

    @Getter
    private final Integer num;

    public static CommodityStatus toStatus(Integer num){
        for(CommodityStatus status : CommodityStatus.values()){
            if(num.equals(status.getNum())){
                return status;
            }
        }
        throw new BusinessException("未找到相应类型");
    }
}
