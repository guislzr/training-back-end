package com.fc.common.po.status;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 15:59
 */
public enum PayStatus {
    /**
     * 成功
     */
    SUCCESS,
    /**
     * 失败
     */
    FAILURE,
    /**
     * 取消
     */
    CANCEL
}
