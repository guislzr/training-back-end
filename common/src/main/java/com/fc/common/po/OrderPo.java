package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/6 16:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_order")
/**
 * 订单
 */
public class OrderPo {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户
     */

    private Long userId;


    /**
     * 收货地址
     */
    private Long receiverAddressId;

    /**
     * 订单item列表
     * List<Long>
     */
    private String orderItemIds;


    /**
     * 总价
     */
    private Double payment;

    /**
     * 总优惠金额
     */
    private Double discount;

    /**
     * 总押金
     */
    private Double totalGuaranteePrice;

    /**
     * 总租金
     */
    private Double totalRentPrice;

    /**
     * 运费
     */
    private Double transportPrice;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 配送方式
     */
    private String deliveryMode;

    /**
     * 支付时间
     */
    private Date paymentTime ;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 交易完成时间
     */
    private Date endTime;

    /**
     * 交易关闭时间
     */
    private Date closeTime;



    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
