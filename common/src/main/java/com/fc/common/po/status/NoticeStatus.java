package com.fc.common.po.status;

import com.fc.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:24
 */
@AllArgsConstructor
public enum NoticeStatus {
    /**
     * 显示
     */
    SHOW(1),
    /**
     * 隐藏
     */
    HIDE(2);

    @Getter
    private final Integer num;

    public static NoticeStatus toStatus(Integer num){
        for(NoticeStatus status : NoticeStatus.values()){
            if(num.equals(status.getNum())){
                return status;
            }
        }
        throw new BusinessException("未找到相应类型");
    }
}
