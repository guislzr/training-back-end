package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.PayStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 15:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("pay_info")
/**
 * 支付详情
 */
public class PayInfoPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long orderId;
    /**
     * 支付流水号
     */
    private String platformNumber;

    /**
     * 支付状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;

}
