package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 15:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_banner")
/**
 * 首页上方滚动图
 */
public class HomeBannerPo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 滚动图链接
     */
    private String picture;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
