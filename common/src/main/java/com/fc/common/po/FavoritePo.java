package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 12:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_favorite")
/**
 * 收藏商品
 */
public class FavoritePo {

    @TableId(type = IdType.AUTO)
    Long id;

    /**
     * 商品id
     */
    Long commodityId;

    /**
     * 收藏商品的用户id
     */
    Long userId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
