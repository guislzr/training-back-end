package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/2 13:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
/**
 * 用户信息
 */
public class UserPo {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 真实姓名
     */
    private String name;


    /**
     * 昵称
     */
    private String nickname;

    /**
     * 手机号（登陆账号）
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别枚举类
     */
    private Integer gender;

    /**
     * 头像url
     */
    private String avatar;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
