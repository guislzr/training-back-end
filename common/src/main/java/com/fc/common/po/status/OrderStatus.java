package com.fc.common.po.status;

import com.fc.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 15:47
 */
@AllArgsConstructor
public enum OrderStatus {
    /**
     * 待付款
     */
    UNPAY(1),
    /**
     * 待发货
     */
    UNSEND(2),
    /**
     * 待收货
     */
    UNRECEIVE(3),
    /**
     * 正在使用
     */
    USING(4),
    /**
     * 已退还
     */
    HAVEBACK(4),
    /**
     * 已购买
     */
    HAVEBUY(5),
    /**
     * 待评价
     */
    UNCREDIT(6),
    /**
     * 退款/售后
     */
    AFTERSALE(7);

    @Getter
    private final Integer num;

    public static OrderStatus toStatus(Integer num){
        for(OrderStatus status : OrderStatus.values()){
            if(num.equals(status.getNum())){
                return status;
            }
        }
        throw new BusinessException("未找到相应类型");
    }
}
