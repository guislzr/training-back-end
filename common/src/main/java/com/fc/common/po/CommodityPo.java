package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/7 12:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("b_commodity")
/**
 * 商品
 */
public class CommodityPo {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 商店id
     */
    private Long storeId;

    /**
     * 商品名称
     */
    private String commodityName;

    /**
     * 商品在售界面显示的商品标题
     */
    private String title;

    /**
     * 商品图片
     */
    private String subImages;

    /**
     * 商品描述
     */
    private String description;

    /**
     * 商品押金
     */
    private Double guaranteePrice;


    /**
     * 商品租金(计量单位如下)
     */
    private Double rentPrice;

    /**
     * 租金对应的天
     */
    private Integer rentTime;

    /**
     * 库存
     */
    private Long number;

    /**
     * 商品状态
     * 在售，下架，待上架
     */
    private Integer commodityStatus;

    /**
     * 自定义属性（规格）
     * 例如：
     * 颜色：红白蓝
     * 大小： 1 2 3
     * Map<String,List<String>>
     */
    private String uniform;


    /**
     * 自定义参数
     * 生产日期，品牌等
     * Map<String,String>
     */
    private String attribute;


    /**
     * 商品所属类别列表
     * 如生活用品
     * List<String>
     */
    private String type;


    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
