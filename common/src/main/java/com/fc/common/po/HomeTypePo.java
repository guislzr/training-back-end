package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 15:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_model_list")
/**
 * 首页滚动图下面的功能列表
 * 例如：新品发布等
 * 由管理员规定有哪些type
 */
public class HomeTypePo {
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 商品类型
     */
    private String type;

    /**
     * 父类别
     */
    private String pType;


    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
