package com.fc.common.po.status;

import com.fc.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 18:45
 */
@AllArgsConstructor
public enum ChargeMode {
    /**
     * 按件计费
     */
    BYPIECE(1),

    /**
     * 按重量计费
     */
    BYWEIGHT(2);

    @Getter
    private final Integer num;

    public static ChargeMode toMode(Integer num){
        for(ChargeMode mode : ChargeMode.values()){
            if(mode.getNum().equals(num)){
                return mode;
            }
        }
        throw new BusinessException("未找到相应类型");
    }
}
