package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/7 13:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("b_problem_aftersale")
/**
 * 售后
 */
public class ProblemAfterSalePo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 订单商品id
     */
    private Long orderItemId;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 店铺
     */
    private Long storeId;

    /**
     * 问题描述
     */
    private String problemDescription;

    /**
     * 问题是否解决
     */
    private Boolean isSolve;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
