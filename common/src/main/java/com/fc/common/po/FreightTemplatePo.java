package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.ChargeMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 18:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("freight_template")
/**
 * 运费模板
 */
public class FreightTemplatePo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 计费方式
     */
    private Integer chargeMode;

    /**
     * 排序
     */
    private Integer rank;

    /**
     * 是否使用
     */
    private Boolean isUsing;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
