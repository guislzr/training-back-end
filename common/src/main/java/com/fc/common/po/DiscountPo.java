package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 16:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("c_discount")
/**
 * 优惠券
 */
public class DiscountPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long storeId;

    /**
     * 优惠券使用的门槛值
     */
    private Double thresholdPrice;

    /**
     * 优惠金额
     * eg. 满200减40 thresholdPrice = 200; discountPrice = 40
     */
    private Double discountPrice;

    /**
     * 可使用起止日期
     */
    private Date startDate;

    /**
     * 使用截至日期
     */
    private Date expiryDate;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
