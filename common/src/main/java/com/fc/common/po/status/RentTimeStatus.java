package com.fc.common.po.status;

import com.fc.common.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/10 15:48
 */
@AllArgsConstructor
public enum RentTimeStatus {
    /**
     * 小时
     */
    HOUR(1),
    /**
     * 天
     */
    DAY(2),
    /**
     * 月
     */
    MONTH(3);

    @Getter
    private final Integer num;

    public static RentTimeStatus toStatus(Integer num){
        for(RentTimeStatus status : RentTimeStatus.values()){
            if(num.equals(status.getNum())){
                return status;
            }
        }
        throw new BusinessException("未找到相应类型");
    }
}
