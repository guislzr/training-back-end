package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 16:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("report")
/**
 * 举报
 */
public class ReportPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long storeId;

    private Long commodityId;
    /**
     * 标签
     */
    private String labels;

    /**
     * 内容
     */
    private String content;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;//为0表示被删了,默认为1
}
