package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 11:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("order_item")
public class OrderItemPo {

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户
     */

    private Long userId;

    /**
     * 商家
     */
    private Long storeId;


    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 数量
     */
    private Integer number;

    /**
     * 租用天数
     */
    private Integer rentDays;

    /**
     * 商品的自定义属性
     * 例如：
     * 颜色：红白蓝
     * 大小： 1 2 3
     * Map<String,String>
     */
    private String uniform;

    /**
         * 优惠券
     */
    private Double discount;

    /**
     * 运费
     */
    private Double transportPrice;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 支付时间
     */
    private Date paymentTime ;

    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 交易完成时间
     */
    private Date endTime;

    /**
     * 交易关闭时间
     */
    private Date closeTime;



    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
