package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 16:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("messages")
/**
 * 消息
 */
public class MessagePo {
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 商店id
     */
    private Long storeId;

    /**
     * 发送者id
     */
    private Long sendId;

    /**
     * 接收者id
     */
    private Long receiverId;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;

}
