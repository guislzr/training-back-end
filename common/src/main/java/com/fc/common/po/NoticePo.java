package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.NoticeStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("notice")
/**
 * 店铺推送给订阅用户的通知
 */
public class NoticePo {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 推送标题
     */
    private String title;

    /**
     * 推送内容
     */
    private String content;

    /**
     * 阅读量
     */
    private Long readNumber;

    /**
     * 推送量
     */
    private Long pushNumber;

    /**
     * 状态（是否隐藏）
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
