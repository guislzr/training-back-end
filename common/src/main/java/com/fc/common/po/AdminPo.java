package com.fc.common.po;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:27
 */

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 店铺管理员
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("admin")
public class AdminPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 管理员所属店铺id
     */
    private Long storeId;

    /**
     * 管理员账号id
     */
    private Long adminId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;


    @TableLogic
    private Integer deleteStatus;
}
