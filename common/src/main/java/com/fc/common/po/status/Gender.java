package com.fc.common.po.status;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/2 13:49
 */


public enum Gender {
    /**
     *男
     */
    MALE,
    /**
     *女
     */
    FEMALE,
    /**
     *未知
     */
    UNKNOWN
}
