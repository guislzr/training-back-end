package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/10 16:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("favorite_commodity")
/**
 * 收藏商品
 */
public class FavoriteCommodityPo {
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long userId;

    private  Long commodityId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
