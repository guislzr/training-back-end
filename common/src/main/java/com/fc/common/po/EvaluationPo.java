package com.fc.common.po;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/7 13:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("b_evaluation")
/**
 * 评价，评论
 */
public class EvaluationPo {
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 评价人id
     */
    private Long userId;

    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 图片
     * List<String>
     */
    private String pictures;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtUpdate;

    @TableLogic
    private Integer deleteStatus;
}
