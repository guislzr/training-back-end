package com.fc.common.interceptor;

import com.fc.common.context.AdminSecurityContext;
import com.fc.common.context.AdminSecurityContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lzr
 * @date 2021/7/10 13:58
 */
public class AuthInterceptor implements HandlerInterceptor {

//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        Cookie[] cookies = request.getCookies();
//        if (cookies == null || cookies.length <= 0) {
//            request.setAttribute("msg", "请先登录！");
//            request.getRequestDispatcher("/login").forward(request, response);
//            return false;
//        }
//        for (Cookie cookie : cookies) {
//            if (cookie.getName().equals("userId")){
//                Long userId = Long.parseLong(cookie.getValue());
//                AdminSecurityContextHolder.setContext(new AdminSecurityContext(userId));
//                return true;
//            }
//        }
//
//        return false;
//    }
}
