package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/4 14:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeatCommodity {
    private Long commodityId;

    private String commodityName;

    private Integer count;
}
