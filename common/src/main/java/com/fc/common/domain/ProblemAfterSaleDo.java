package com.fc.common.domain;

import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 22:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProblemAfterSaleDo {
    private Long id;

    /**
     * 订单商品id
     */
    private Long orderItemId;

    /**
     * 顾客id
     */
    private Long customerId;

    /**
     * 顾客昵称
     */
    private String customerNickName;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 总价
     */
    private Double payment;

    /**
     * 问题描述
     */
    private String problemDescription;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 问题是否解决
     */
    private Boolean isSolve;
}
