package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationDo {
    private Long id;
    /**
     * 评价人id
     */
    private Long userId;

    /**
     * 商品id
     */
    private Long commodityId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评分
     */
    private Integer score;

    /**
     * 图片
     */
    private List<String> pictures;

    /**
     * 评价时间
     */
    private Date time;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;
}
