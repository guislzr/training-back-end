package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlbumDo {
    /**
     * id
     */
    private Long id;


    /**
     * 商店id
     */
    private String storeId;

    /**
     * 相册名称
     */
    private String name;

    /**
     * 图片
     */
    private List<String> pictures;
}
