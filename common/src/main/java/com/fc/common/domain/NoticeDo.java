package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fc.common.po.status.NoticeStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeDo {
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 推送标题
     */
    private String title;

    /**
     * 推送内容
     */
    private String content;

    /**
     * 阅读量
     */
    private Long readNumber;

    /**
     * 推送量
     */
    private Long pushNumber;

    /**
     * 状态（是否隐藏）
     */
    private NoticeStatus status;

    /**
     * 发布时间
     */
    private Date createTime;
}
