package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 首页滚动图
 */
public class HomeBannerDo {
    /**
     * id
     */
    private Long id;

    /**
     * 滚动图链接
     *
     */
    private String picture;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;

}
