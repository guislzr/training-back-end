package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/20 11:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportDo {

    private Long id;

    private Long userId;

    private Long storeId;

    private Long commodityId;

    private List<String> labels;

    private String content;

    private Date gmtCreate;

    private Date gmtUpdate;
}
