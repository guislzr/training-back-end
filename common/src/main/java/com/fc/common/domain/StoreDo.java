package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/22 21:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StoreDo {

    private Long id;

    private String icon;

    /**
     * 店主id
     */
    private Integer storekeeperId;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 店铺评分
     */
    private Double score;

    /**
     * 信誉积分
     */
    private Integer creditScore;

    /**
     * 店铺介绍
     */
    private String introduction;
}
