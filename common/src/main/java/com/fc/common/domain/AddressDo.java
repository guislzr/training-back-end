package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDo {

    private Long id;

    private Long  userId;

    private String receiverName;

    private String receiverPhone;

    private String country;

    private String province;

    private String city;

    private String district;

    private String address;

    private String zip;

    private Boolean isDefault;

    private Date gmtCreate;

    private Date gmtUpdate;

}
