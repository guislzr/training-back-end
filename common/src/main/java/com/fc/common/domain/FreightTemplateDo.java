package com.fc.common.domain;

import com.fc.common.po.status.ChargeMode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FreightTemplateDo {
    private Long id;

    /**
     * 店铺id
     */
    private Long storeId;

    /**
     * 模板名称
     */
    private String templateName;

    /**
     * 计费方式
     */
    private ChargeMode chargeMode;

    /**
     * 排序
     */
    private Integer rank;

    /**
     * 是否使用
     */
    private Boolean isUsing;

    private Date createTime;
}
