package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 20:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminDo {
    private Long id;

    /**
     * 管理员所属店铺id
     */
    private Long storeId;

    /**
     * 管理员账号id
     */
    private Long adminId;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 用户头像
     */
    private String icon;

    /**
     * 手机号（登陆账号）
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;


}
