package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 13:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteStoreDo {

    private Long id;

    private Long userId;

    private  Long storeId;

    private Date subscribeDate;

    private Date gmtCreate;

    private Date gmtUpdate;
}
