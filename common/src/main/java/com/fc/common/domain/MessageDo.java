package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 消息通知
 */
public class MessageDo {
    private Long id;
    /**
     * 商店id
     */
    private Long storeId;

    /**
     * 发送者id
     */
    private Long sendId;

    /**
     * 接收者id
     */
    private Long receiverId;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;

}
