package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/23 16:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 首页商品类型
 */
public class HomeTypeDo {
    private Long id;


    /**
     * 商品类型
     */
    private String type;

    /**
     * 类型图标
     */
    private String pType;


    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

}
