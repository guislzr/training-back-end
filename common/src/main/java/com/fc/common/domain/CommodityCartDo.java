package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 11:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommodityCartDo {
    private Long id;

    /**
     * 商品id
     * k--商品id
     * v--数量
     * Map<Long,Long>
     */
    private Map<Long,Long> commodityIds;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 收货地址
     */
    private Long receiverAddressId;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;
}
