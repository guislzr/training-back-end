package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 订单
 */
public class OrderDo {
    private Long id;

    /**
     * 用户
     */

    private Long userId;


    /**
     * 收货地址
     */
    private Long receiverAddressId;

    /**
     * 运费
     */
    private Double transportPrice;


    /**
     * 订单item列表
     * List<Long>
     */
    private List<Long> orderItemIds;


    /**
     * 总优惠金额
     */
    private Double discount;


    /**
     * 订单状态
     */
    private OrderStatus status;

    /**
     * 配送方式
     */
    private String deliveryMode;


    /**
     * 支付时间
     */
    private Date paymentTime;

    /**
     * 发货时间
     */
    private Date sendTim;

    /**
     * 交易完成时间
     */
    private Date endTime;

    /**
     * 交易关闭时间
     */
    private Date closeTime;


    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;

}
