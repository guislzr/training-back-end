package com.fc.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 12:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FavoriteDo {

    Long id;

    /**
     * 商品id
     */
    Long commodityId;

    /**
     * 收藏商品的用户id
     */
    Long userId;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 更新时间
     */
    private Date gmtUpdate;
}
