package com.fc.common.domain;

import com.fc.common.po.status.CommodityStatus;
import com.fc.common.po.status.RentTimeStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author lzr
 * @date 2021/3/18 14:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 商品
 */
public class CommodityDo {

    /**
     * id
     */
    private Long id;


    /**
     * 商店id
     */
    private Long storeId;

    /**
     * 商品名称
     */
    private String commodityName;

    /**
     * 商品在售界面显示的商品标题
     */
    private String title;

    /**
     * 商品图片
     */
    private String subImages;

    /**
     * 商品描述
     */
    private String description;


    private Double guaranteePrice;

    /**
     * 商品租金(计量单位如下)
     */
    private Double rentPrice;

    /**
     * 租金对应的天
     */
    private RentTimeStatus rentTime;

    /**
     * 租金对应的天
     */
    private Integer rentDays;

    /**
     * 库存
     */
    private Long number;

    /**
     * 商品状态
     * 在售，下架，待上架
     */
    private CommodityStatus commodityStatus;

    /**
     * 自定义属性
     * 例如：
     * 颜色：红白蓝
     * 大小： 1 2 3
     */
    private Map<String,List<String>> uniform;



    /**
     * 自定义参数
     * 生产日期，品牌等
     * */
    private Map<String,String> attribute;




    /**
     * 用户评论单独一个表。
     */

    /**
     * 商品所属类别列表
     * 如生活用品
     */
    private List<String> type;


    private Date gmtCreate;

    private Date gmtUpdate;


}
