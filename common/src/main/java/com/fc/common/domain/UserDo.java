package com.fc.common.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fc.common.po.status.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 真实姓名
     */
    private String name;


    /**
     * 昵称
     */
    private String nickname;

    /**
     * 手机号（登陆账号）
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别枚举类
     */
    private Integer gender;

    /**
     * 头像url
     */
    private String avatar;

    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 更新时间
     */
    private Date gmtUpdate;

}
