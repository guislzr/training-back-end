package com.fc.business;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fc.business.service.AdminService;
import com.fc.business.service.CommodityService;
import com.fc.common.domain.AdminDo;
import com.fc.common.po.OrderPo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
class BusinessServiceApplicationTests {
    @Autowired
    AdminService adminService;

    @Autowired
    private CommodityService commodityService;

    @Test
    void contextLoads() {
        System.out.println(commodityService.selectByStoreId(0L, 1, 10));
    }

    @Test
    void add() {
        ApplicationContext context = new AnnotationConfigApplicationContext();
        adminService = (AdminService) context.getBean("adminServiceImpl");
        AdminDo adminDo = new AdminDo();
        adminDo.setAdminId(1L);
        adminDo.setStoreId(2L);
        adminService.create(adminDo);
    }

    @Test
    void query() {
        System.out.println(adminService.selectByStoreId(2L));
    }

}
