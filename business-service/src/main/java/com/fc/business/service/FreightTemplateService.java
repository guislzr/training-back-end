package com.fc.business.service;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.FreightTemplateDo;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:18
 */
public interface FreightTemplateService {

    /**
     * 创建模板
     * @param freightTemplateDo 模板do
     */
    void create(FreightTemplateDo freightTemplateDo);

    /**
     * 通过id删除模板
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新模板
     * @param freightTemplateDo 模板do
     */
    void update(FreightTemplateDo freightTemplateDo);

    FreightTemplateDo selectById(Long id);

    /**
     * 查询所有模板
     */
    List<FreightTemplateDo> selectByStoreId(Long storeId);

}
