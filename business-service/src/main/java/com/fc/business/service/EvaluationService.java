package com.fc.business.service;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.EvaluationDo;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 15:00
 */
public interface EvaluationService {
    /**
     * 新建评论
     * @param evaluationDo 评论do
     */
    void create(EvaluationDo evaluationDo);

    /**
     * 通过id删除评论
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 查询单条评论
     * @param id 评论id
     * @return 评论do
     */
    EvaluationDo selectById(Long id);

    /**
     * 查询某个商品的所有评论
     * @param commodityId 商品id
     * @return 商品do列表
     */
    Map<String, Object> selectByCommodityId(Long commodityId, Integer page, Integer size);
}
