package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.business.facade.OrderItemFacade;
import com.fc.business.service.CommodityService;
import com.fc.business.service.ProblemService;
import com.fc.common.convert.ProblemDataConvert;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.domain.ProblemAfterSaleDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.OrderItemMapper;
import com.fc.common.mapper.ProblemMapper;
import com.fc.common.mapper.UserMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.ProblemAfterSalePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 15:30
 */
@Service
public class ProblemServiceImpl implements ProblemService {
    @Resource
    private ProblemMapper problemMapper;

    @Resource
    private ProblemDataConvert problemDataConvert;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private CommodityService commodityService;

    @Resource
    private UserMapper userMapper;


    @Override
    public void create(ProblemAfterSaleDo problemAfterSaleDo) {
        ProblemAfterSalePo problemAfterSalePo = problemDataConvert.convert(problemAfterSaleDo);
        problemMapper.insert(problemAfterSalePo);
    }

    @Override
    public void deleteById(Long id) {
        int i = problemMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(ProblemAfterSaleDo problemAfterSaleDo) {
        int update = problemMapper.updateById(problemDataConvert.convert(problemAfterSaleDo));
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public void solveProblemById(Long id) {
        ProblemAfterSalePo problemAfterSalePo = problemMapper.selectById(id);
        problemAfterSalePo.setIsSolve(true);
        int update = problemMapper.update(problemAfterSalePo, null);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public ProblemAfterSaleDo selectById(Long id) {
        ProblemAfterSalePo problemAfterSalePo = problemMapper.selectById(id);
        return problemDataConvert.convert(problemAfterSalePo);
    }

    @Override
    public List<ProblemAfterSaleDo> selectByStoreId(Long storeId) {
        QueryWrapper<ProblemAfterSalePo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId);
        List<ProblemAfterSalePo> problemAfterSalePos = problemMapper.selectList(wrapper);
        return listAddInfo(problemAfterSalePos.stream().map(problemAfterSalePo -> problemDataConvert.convert(problemAfterSalePo))
                .collect(Collectors.toList()));
    }

    @Override
    public List<ProblemAfterSaleDo> selectByIsSolve(Long storeId, Boolean isSolve) {
        QueryWrapper<ProblemAfterSalePo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId).eq("is_solve", isSolve);
        List<ProblemAfterSalePo> problemAfterSalePos = problemMapper.selectList(wrapper);
        return listAddInfo(problemAfterSalePos.stream().map(problemAfterSalePo -> problemDataConvert.convert(problemAfterSalePo))
                .collect(Collectors.toList()));
    }

    @Override
    public List<ProblemAfterSaleDo> selectByCustomerId(Long storeId, Long customerId) {
        QueryWrapper<ProblemAfterSalePo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId).eq("customer_id", customerId);
        List<ProblemAfterSalePo> problemAfterSalePos = problemMapper.selectList(wrapper);
        return listAddInfo(problemAfterSalePos.stream().map(problemAfterSalePo -> problemDataConvert.convert(problemAfterSalePo))
                .collect(Collectors.toList()));
    }

    private List<ProblemAfterSaleDo> listAddInfo(List<ProblemAfterSaleDo> oriProblemList) {
        oriProblemList.forEach((ProblemAfterSaleDo problemAfterSaleDo) -> {
            OrderItemPo orderItem = orderItemMapper.selectById(problemAfterSaleDo.getOrderItemId());
            CommodityPo commodityPo = commodityService.getById(orderItem.getCommodityId());
            if (Objects.nonNull(commodityPo)) {
                problemAfterSaleDo.setPayment(orderItem.getRentDays() * orderItem.getNumber() * commodityPo.getRentPrice());
            }
            String nickName = userMapper.selectById(problemAfterSaleDo.getCustomerId()).getNickname();
            problemAfterSaleDo.setCustomerNickName(nickName);
        });
        return oriProblemList;
    }


}
