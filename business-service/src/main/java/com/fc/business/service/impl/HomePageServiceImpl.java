package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fc.business.service.HomePageService;
import com.fc.common.domain.HeatCommodity;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.FavoriteStoreMapper;
import com.fc.common.mapper.OrderItemMapper;
import com.fc.common.mapper.OrderMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.FavoriteStorePo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.CommodityStatus;
import com.fc.common.po.status.OrderStatus;
import com.fc.common.vo.BOrderStatisticVo;
import com.fc.common.vo.BStoreStatisticVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/2 15:32
 */
@Service
public class HomePageServiceImpl implements HomePageService {
    @Resource
    private OrderMapper orderMapper;

    @Resource
    private CommodityMapper commodityMapper;

    @Resource
    private FavoriteStoreMapper favoriteStoreMapper;

    @Resource
    private OrderItemMapper orderItemMapper;

    @Override
    public BOrderStatisticVo getOrderStatistic(Long storeId) {
        BOrderStatisticVo orderStatisticVo = new BOrderStatisticVo();
        QueryWrapper<OrderPo> wrapper = Wrappers.query();
        wrapper.eq("storeId",storeId);

        orderStatisticVo.setTotalOrderNumber(orderMapper.selectCount(wrapper).longValue());
        orderStatisticVo.setMonthOrderNumber(orderMapper.queryMonthOrderNumber(storeId));
        orderStatisticVo.setMonthTradeAmount(orderMapper.queryMonthOrderAmount(storeId));
        orderStatisticVo.setTodayTradeAmount(orderMapper.queryTodayOrderAmount(storeId));
        orderStatisticVo.setYesterdayOrderNumber(orderMapper.queryOrderNumber(storeId,1));
        orderStatisticVo.setYesterdayTradeAmount(orderMapper.queryYesterdayOrderAmount(storeId));
        List<Integer> orderNumbers = new  ArrayList<>();
        for (int i=6; i >= 0; i--){
            orderNumbers.add(orderMapper.queryOrderNumber(storeId,i));
        }
        orderStatisticVo.setLastWeekOrderNumbers(orderNumbers);
        Map<OrderStatus,Integer> orderNumberMap = new HashMap<>(8);
        for (OrderStatus status:OrderStatus.values()){

            wrapper.eq("store_id",storeId).eq("status",status);
            orderNumberMap.put(status,orderMapper.selectCount(wrapper));
        }
        orderStatisticVo.setOrderOfStatus(orderNumberMap);
        return orderStatisticVo;
    }

    @Override
    public BStoreStatisticVo getStoreStatistic(Long storeId) {
        BStoreStatisticVo storeStatisticVo = new BStoreStatisticVo();
        QueryWrapper<FavoriteStorePo> wrapper = Wrappers.query();
        wrapper.eq("store_id",storeId);
        storeStatisticVo.setCollectors(favoriteStoreMapper.selectCount(wrapper).longValue());
        QueryWrapper<CommodityPo>  commodityWrapper = Wrappers.query();
        commodityWrapper.eq("store_id",storeId).eq("commodity_status", CommodityStatus.ONSALE);
        storeStatisticVo.setOnSaleCommodity(commodityMapper.selectCount(commodityWrapper));

        commodityWrapper = Wrappers.query();
        commodityWrapper.eq("store_id",storeId).eq("commodity_status", CommodityStatus.WAIT);
        storeStatisticVo.setWaitCommodity(commodityMapper.selectCount(commodityWrapper));


        List<HeatCommodity> heatCommodities = orderItemMapper.selectHeatCommodity(storeId);
        heatCommodities.forEach(commodity -> commodity.setCommodityName(commodityMapper.selectById(commodity.getCommodityId()).getCommodityName()));
        storeStatisticVo.setHeatCommodities(heatCommodities);

        return storeStatisticVo;

    }
}
