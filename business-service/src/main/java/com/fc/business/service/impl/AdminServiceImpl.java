package com.fc.business.service.impl;

import com.fc.business.service.AdminService;
import com.fc.common.convert.AdminDataConvert;
import com.fc.common.domain.AdminDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.AdminMapper;
import com.fc.common.po.AdminPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 17:25
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Resource
    private AdminMapper adminMapper;

    @Resource
    private AdminDataConvert adminDataConvert;


    @Override
    public void create(AdminDo adminDo) {
        AdminPo adminPo = adminDataConvert.convert(adminDo);
        adminMapper.insert(adminPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = adminMapper.deleteById(id);
        if (i<=0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(AdminDo adminDo) {
        int update = adminMapper.updateById(adminDataConvert.convert(adminDo));
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public List<AdminDo> selectByStoreId(Long storeId) {
        return adminMapper.selectAdminByStore(storeId);
    }
}
