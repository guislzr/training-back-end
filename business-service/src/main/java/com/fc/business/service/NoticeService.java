package com.fc.business.service;

import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.NoticeDo;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 16:10
 */
public interface NoticeService {
    /**
     * 新建推送
     * @param noticeDo 评论do
     */
    void create(NoticeDo noticeDo);

    /**
     * 通过id删除推送
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 通过商店id查询推送
     * @param storeId 商店id
     * @param page 页码
     * @param size 大小
     */
    Map<String, Object> selectByStoreId(Long storeId, Integer page, Integer size);
}
