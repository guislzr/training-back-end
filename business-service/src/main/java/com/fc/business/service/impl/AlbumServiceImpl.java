package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.business.service.AlbumService;
import com.fc.common.convert.AlbumDataConvert;
import com.fc.common.domain.AdminDo;
import com.fc.common.domain.AlbumDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.AlbumMapper;
import com.fc.common.po.AdminPo;
import com.fc.common.po.AlbumPo;
import com.fc.common.po.CommodityPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 20:07
 */
@Service
public class AlbumServiceImpl implements AlbumService {
    @Resource
    private AlbumMapper albumMapper;

    @Resource
    private AlbumDataConvert albumDataConvert;


    @Override
    public void create(AlbumDo albumDo) {
        AlbumPo albumPo = albumDataConvert.convert(albumDo);
        albumMapper.insert(albumPo);

    }

    @Override
    public void deleteById(Long id) {
        int i = albumMapper.deleteById(id);
        if(i<=0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(AlbumDo albumDo) { ;
        int update = albumMapper.updateById(albumDataConvert.convert(albumDo));
        if (update <= 0){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public void addPicture(Long id, String picture) {
        AlbumDo albumDo = albumDataConvert.convert(albumMapper.selectById(id));
        List<String> pictures = albumDo.getPictures();
        pictures.add(picture);
        albumDo.setPictures(pictures);
        update(albumDo);
    }

    @Override
    public void deletePicture(Long id, Integer pictureOrder) {
        AlbumDo albumDo = albumDataConvert.convert(albumMapper.selectById(id));
        List<String> pictures = albumDo.getPictures();
        if (pictureOrder<=pictures.size()) {
            pictures.remove(pictureOrder - 1);
        }
        else {
            throw new BusinessException("当前序号的照片不存在");
        }
        albumDo.setPictures(pictures);
        update(albumDo);
    }

    @Override
    public List<AlbumDo> selectByStoreId(Long storeId) {
        QueryWrapper<AlbumPo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId);
        List<AlbumPo> albumPos = albumMapper.selectList(wrapper);
        List<AlbumDo> albumDos = albumPos.stream().map(albumPo -> albumDataConvert.convert(albumPo))
                .collect(Collectors.toList());
        for (AlbumDo albumDo:albumDos){
            List<String> pictures = new ArrayList<>();
            pictures.add(albumDo.getPictures().get(0));
            albumDo.setPictures(pictures);
        }
        return albumDos;
    }

    @Override
    public AlbumDo selectById(Long id) {
        AlbumPo albumPo = albumMapper.selectById(id);
        return albumDataConvert.convert(albumPo);
    }
}
