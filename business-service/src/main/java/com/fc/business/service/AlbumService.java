package com.fc.business.service;

import com.fc.common.domain.AdminDo;
import com.fc.common.domain.AlbumDo;
import com.fc.common.domain.CommodityDo;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 19:58
 */
public interface AlbumService {
    /**
     * 新建相册
     * @param albumDo 相册do
     */
    void create(AlbumDo albumDo);

    /**
     * 通过id删除整个相册
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新相册
     * @param albumDo 相册do
     */
    void update(AlbumDo albumDo);

    /**
     * 添加照片
     * @param id 相册id
     * @param picture 单张照片
     */
    void addPicture(Long id, String picture);


    /**
     * 删除相册里的一张照片
     * @param id 相册id
     * @param pictureOrder 照片排序
     */
    void deletePicture(Long id,Integer pictureOrder);

    /**
     * 根据商店id查询相册列表
     * @param storeId 商店id
     * @return 商店相册列表(只包含每个相册的第一张图片)
     */
    List<AlbumDo> selectByStoreId(Long storeId);

    /**
     * 根据相册id查询相册
     * @param id 相册id
     * @return 相册
     */
    AlbumDo selectById(Long id);
}
