package com.fc.business.service;

import com.fc.common.vo.BOrderStatisticVo;
import com.fc.common.vo.BStoreStatisticVo;

import java.util.Date;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/2 15:31
 */
public interface HomePageService {
    /**
     * 获取首页订单相关数据
     * @param storeId 商店id
     * @return 订单相关数据
     */
    BOrderStatisticVo getOrderStatistic(Long storeId);

    /**
     * 获取首页商店相关数据
     * @param storeId 商店id
     * @return 商店相关数据
     */
    BStoreStatisticVo getStoreStatistic(Long storeId);
}
