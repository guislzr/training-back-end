package com.fc.business.service;

import com.fc.common.domain.AdminDo;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.NoticeDo;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 17:25
 */
public interface AdminService {
    /**
     * 新建管理员
     * @param adminDo 管理员do
     */
    void create(AdminDo adminDo);

    /**
     * 通过id删除管理员
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新管理员
     * @param adminDo 管理员do
     */
    void update(AdminDo adminDo);

    /**
     * 通过商店id查询推送
     * @param storeId 商店id
     */
    List<AdminDo> selectByStoreId(Long storeId);

}
