package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.business.service.StoreService;
import com.fc.common.context.AdminSecurityContextHolder;
import com.fc.common.convert.StoreDataConvert;
import com.fc.common.convert.StoreDomainConvert;
import com.fc.common.domain.StoreDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.StoreMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.NoticePo;
import com.fc.common.po.StorePo;
import com.fc.common.vo.NoticeVo;
import com.fc.common.vo.StoreVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 14:32
 */
@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, StorePo> implements StoreService {
    @Resource
    private StoreMapper storeMapper;

    @Resource
    private StoreDataConvert storeDataConvert;

    @Resource
    private StoreDomainConvert storeDomainConvert;

    @Override
    public void create(StoreDo storeDo) {
        StorePo storePo = storeDataConvert.convert(storeDo);
        storePo.setStorekeeperId(AdminSecurityContextHolder.getAdminId());
        storeMapper.insert(storePo);
    }

    @Override
    public void deleteById(Long id) {
        int i = storeMapper.deleteById(id);
        if(i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(StoreDo storeDo) {
        int update = storeMapper.updateById(storeDataConvert.convert(storeDo));
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public StoreDo selectById(Long id) {
        StorePo storePo = storeMapper.selectById(id);
        return storeDataConvert.convert(storePo);
    }

    @Override
    public StoreDo selectByKeeperId(Long storekeeperId) {
        QueryWrapper<StorePo> wrapper = new QueryWrapper<>();
        wrapper.eq("storekeeper_id", storekeeperId);
        List<StorePo> storePos = storeMapper.selectList(wrapper);

        List<StoreDo> storeDos = storePos.stream().map(storePo -> storeDataConvert.convert(storePo))
                .collect(Collectors.toList());
        if (storeDos.size()>1){
            throw new BusinessException("该账户拥有多个店铺");
        }
        else if(storeDos.size()==1){
            return storeDos.get(0);
        }
        else {
            return null;
        }
    }

    @Override
    public Map<String, Object> searchByStoreName(String  storeName, Integer page, Integer size) {
        QueryWrapper<StorePo> storeWrapper = new QueryWrapper<>();
        storeWrapper.like("store_id", storeName).orderByDesc("gmt_create");
        Page<StorePo> storePoPage = new Page<>(page - 1, size);
        IPage<StorePo> storePoIPage = storeMapper.selectPage(storePoPage, storeWrapper);

        List<StoreVo> records = storePoIPage.getRecords().stream().map(storePo -> storeDataConvert.convert(storePo))
                .map(storeDo -> storeDomainConvert.convert(storeDo))
                .collect(Collectors.toList());

        return new HashMap<String, Object>(){
            {
                put("records", records);
                put("totalPages", storePoIPage.getPages());
                put("current", storePoIPage.getCurrent());
            }
        };
    }

    @Override
    public Map<String, Object> getList(Integer pageNum, Integer pageSize) {
        Page<StorePo> page = page(new Page<>(pageNum, pageSize));
        List<StoreVo> records = page.getRecords().stream().map(storePo -> storeDataConvert.convert(storePo))
                .map(storeDo -> storeDomainConvert.convert(storeDo))
                .collect(Collectors.toList());

        return new HashMap<String, Object>(){
            {
                put("records", records);
                put("totalPages", page.getPages());
                put("current", page.getCurrent());
            }
        };
    }
}
