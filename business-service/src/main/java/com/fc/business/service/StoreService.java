package com.fc.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.StoreDo;
import com.fc.common.po.StorePo;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 14:20
 */
public interface StoreService extends IService<StorePo> {
    /**
     * 创建新店铺
     * @param storeDo 店铺do
     */
    void create(StoreDo storeDo);

    /**
     * 通过id删除店铺
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新店铺信息
     * @param storeDo 店铺do
     */
    void update(StoreDo storeDo);

    StoreDo selectById(Long id);

    /**
     * 根据店铺名查询店铺
     * @param storekeeperId 店主id
     * @return 店铺列表
     */
    StoreDo selectByKeeperId(Long storekeeperId);

    /**
     * 通过商店名称搜索
     * @param storeName 商店名称
     * @param page 页码
     * @param size 大小
     */
    Map<String, Object> searchByStoreName(String storeName, Integer page, Integer size);

    Map<String, Object> getList(Integer pageNum, Integer pageSize);
}
