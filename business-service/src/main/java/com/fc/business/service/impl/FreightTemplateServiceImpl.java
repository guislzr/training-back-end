package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.business.service.FreightTemplateService;
import com.fc.common.convert.FreightTemplateDataConvert;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.FreightTemplateDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.FreightTemplateMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.FreightTemplatePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/29 19:32
 */
@Service
public class FreightTemplateServiceImpl implements FreightTemplateService {
    @Resource
    private FreightTemplateMapper freightTemplateMapper;

    @Resource
    private FreightTemplateDataConvert freightTemplateDataConvert;


    @Override
    public void create(FreightTemplateDo freightTemplateDo) {
        FreightTemplatePo freightTemplatePo = freightTemplateDataConvert.convert(freightTemplateDo);
        freightTemplateMapper.insert(freightTemplatePo);
    }

    @Override
    public void deleteById(Long id) {
        int i = freightTemplateMapper.deleteById(id);
        if (i<=0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(FreightTemplateDo freightTemplateDo) {
        int update = freightTemplateMapper.updateById(freightTemplateDataConvert.convert(freightTemplateDo));
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public FreightTemplateDo selectById(Long id) {
        FreightTemplatePo freightTemplatePo = freightTemplateMapper.selectById(id);
        return freightTemplateDataConvert.convert(freightTemplatePo);
    }

    @Override
    public List<FreightTemplateDo> selectByStoreId(Long storeId) {
        QueryWrapper<FreightTemplatePo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId);
        List<FreightTemplatePo> freightTemplatePos = freightTemplateMapper.selectList(wrapper);
        return freightTemplatePos.stream().map(freightTemplatePo -> freightTemplateDataConvert.convert(freightTemplatePo))
                .collect(Collectors.toList());
    }
}
