package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fc.business.service.EvaluationService;
import com.fc.common.context.AdminSecurityContextHolder;
import com.fc.common.convert.EvaluationDataConvert;
import com.fc.common.convert.EvaluationDomainConvert;
import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.UserDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.EvaluationMapper;
import com.fc.common.mapper.UserMapper;
import com.fc.common.po.EvaluationPo;
import com.fc.common.po.UserPo;
import com.fc.common.vo.EvaluationVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 14:59
 */
@Service
public class EvaluationServiceImpl implements EvaluationService {
    @Resource
    private EvaluationMapper evaluationMapper;

    @Resource
    private EvaluationDataConvert evaluationDataConvert;

    @Resource
    private EvaluationDomainConvert evaluationDomainConvert;

    @Autowired
    private UserMapper userMapper;

    @Override
    public void create(EvaluationDo evaluationDo) {
        EvaluationPo evaluationPo = evaluationDataConvert.convert(evaluationDo);;
//        evaluationPo.setUserId(AdminSecurityContextHolder.getAdminId());
        evaluationMapper.insert(evaluationPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = evaluationMapper.deleteById(id);
        if (i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public EvaluationDo selectById(Long id) {
        EvaluationPo evaluationPo = evaluationMapper.selectById(id);
        return evaluationDataConvert.convert(evaluationPo);
    }

    @Override
    public Map<String, Object> selectByCommodityId(Long commodityId, Integer page, Integer size) {
//        QueryWrapper<EvaluationPo> wrapper = new QueryWrapper<>();
//        wrapper.eq("commodityId", commodityId);
//        List<EvaluationPo> evaluationPos = evaluationMapper.selectList(wrapper);
//        return evaluationPos.stream().map(evaluationPo -> evaluationDataConvert.convert(evaluationPo))
//                .collect(Collectors.toList());
        QueryWrapper<EvaluationPo> evaluationWrapper = new QueryWrapper<>();
        evaluationWrapper.eq("commodity_id", commodityId).orderByDesc("gmt_create");
        Page<EvaluationPo> evaluationPoPage = new Page<>(page - 1, size);
        IPage<EvaluationPo> evaluationPoIPage = evaluationMapper.selectPage(evaluationPoPage, evaluationWrapper);

        List<EvaluationDo> records = evaluationPoIPage.getRecords().stream().map(evaluationPo -> evaluationDataConvert.convert(evaluationPo))
                .collect(Collectors.toList());
        List<EvaluationVo> evaluationVos = records.stream().map(evaluationDo -> {
            EvaluationVo evaluationVo = new EvaluationVo();
            BeanUtils.copyProperties(evaluationDo, evaluationVo);
            UserPo userPo = userMapper.selectById(evaluationDo.getUserId());
            evaluationVo.setUserNickname(userPo.getNickname());
            return evaluationVo;
        }).collect(Collectors.toList());

        return new HashMap<String, Object>(){
            {
                put("records", evaluationVos);
                put("totalPages", evaluationPoIPage.getPages());
                put("current", evaluationPoIPage.getCurrent());
            }
        };
    }
}
