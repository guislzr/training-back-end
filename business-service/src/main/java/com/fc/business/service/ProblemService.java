package com.fc.business.service;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.EvaluationDo;
import com.fc.common.domain.ProblemAfterSaleDo;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/23 15:30
 */
public interface ProblemService {
    /**
     * 新建售后问题
     * @param problemAfterSaleDo 售后问题do
     */
    void create(ProblemAfterSaleDo problemAfterSaleDo);

    /**
     * 通过id删除售后
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新售后问题信息
     * @param problemAfterSaleDo 售后问题do
     */
    void update(ProblemAfterSaleDo problemAfterSaleDo);


    /**
     * 解决售后问题
     * @param id 售后问题id
     */
    void solveProblemById(Long id);


    ProblemAfterSaleDo selectById(Long id);

    /**
     * 根据商店id查询售后信息
     * @param storeId 商家id
     * @return 售后信息列表
     */
    List<ProblemAfterSaleDo> selectByStoreId(Long storeId);


    /**
     * 根据问题是否解决查询售后信息
     * @param storeId 商家id
     * @param isSolve 问题是否解决
     * @return 售后信息列表
     */
    List<ProblemAfterSaleDo> selectByIsSolve(Long storeId, Boolean isSolve);

    /**
     *  根据商店id和买家id查询售后信息
     * @param storeId 商家id
     * @param customerId 买家id
     * @return 售后信息列表
     */
    List<ProblemAfterSaleDo> selectByCustomerId(Long storeId, Long customerId);
}
