package com.fc.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fc.business.service.NoticeService;
import com.fc.common.convert.NoticeDataConvert;
import com.fc.common.convert.NoticeDomainConvert;
import com.fc.common.domain.NoticeDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.NoticeMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.FreightTemplatePo;
import com.fc.common.po.NoticePo;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.NoticeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/3/31 16:11
 */
@Service
public class NoticeServiceImpl implements NoticeService {
    @Resource
    private NoticeMapper noticeMapper;

    @Resource
    private NoticeDataConvert noticeDataConvert;

    @Resource
    private NoticeDomainConvert noticeDomainConvert;


    @Override
    public void create(NoticeDo noticeDo) {
        NoticePo noticePo = noticeDataConvert.convert(noticeDo);
        noticeMapper.insert(noticePo);
    }

    @Override
    public void deleteById(Long id) {
        int i = noticeMapper.deleteById(id);
        if (i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public Map<String, Object> selectByStoreId(Long storeId, Integer page, Integer size) {
        QueryWrapper<NoticePo> noticeWrapper = new QueryWrapper<>();
        noticeWrapper.eq("store_id", storeId).orderByDesc("gmt_create");
        Page<NoticePo> noticePoPage = new Page<>(page - 1, size);
        IPage<NoticePo> noticePoIPage = noticeMapper.selectPage(noticePoPage, noticeWrapper);

        List<NoticeVo> records = noticePoIPage.getRecords().stream().map(noticePo -> noticeDataConvert.convert(noticePo))
                .map(noticeDo -> noticeDomainConvert.convert(noticeDo))
                .collect(Collectors.toList());

        return new HashMap<String, Object>(){
            {
                put("records", records);
                put("totalPages", noticePoIPage.getPages());
                put("current", noticePoIPage.getCurrent());
            }
        };
    }
}
