package com.fc.business.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.business.service.CommodityService;
import com.fc.common.convert.BCommodityDomainConvert;
import com.fc.common.convert.CommodityDataConvert;
import com.fc.common.domain.CommodityDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.OrderItemMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.OrderItemPo;
import com.fc.common.vo.BCommodityVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author lzr
 * @date 2021/3/18 15:17
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, CommodityPo> implements CommodityService {
    @Resource
    private CommodityMapper commodityMapper;

    @Resource
    private OrderItemMapper orderItemMapper;

    @Resource
    private CommodityDataConvert commodityDataConvert;

    @Resource
    private BCommodityDomainConvert bCommodityDomainConvert;

    @Override
    public void create(CommodityDo commodityDo) {
        CommodityPo convertPo = commodityDataConvert.convert(commodityDo);
        commodityMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = commodityMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(CommodityDo commodityDo) {
        CommodityPo convert = commodityDataConvert.convert(commodityDo);
        convert.setId(commodityDo.getId());
        int update = commodityMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public CommodityDo selectById(Long id) {
        CommodityPo commodityPo = commodityMapper.selectById(id);
        return commodityDataConvert.convert(commodityPo);
    }

    @Override
    public List<CommodityDo> selectByName(String commodityName) {
        QueryWrapper<CommodityPo> wrapper = new QueryWrapper<>();
        wrapper.like("commodity_name", commodityName);
        List<CommodityPo> commodityPos = commodityMapper.selectList(wrapper);
        return commodityPos.stream().map(commodityPo -> commodityDataConvert.convert(commodityPo))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> selectByStoreId(Long storeId, Integer page, Integer size) {
        LambdaQueryWrapper<CommodityPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(CommodityPo::getStoreId, storeId);
        Page<CommodityPo> pages = page(new Page<>(page, size), lambdaQueryWrapper);

        List<BCommodityVo> records = pages.getRecords().stream().map(commodityPo -> commodityDataConvert.convert(commodityPo))
                .map(commodityDo -> {

                    BCommodityVo commodityVo = bCommodityDomainConvert.convert(commodityDo);
                    commodityVo.setReleaseTime(commodityDo.getGmtCreate());
                    LambdaQueryWrapper<OrderItemPo> wrapper = new LambdaQueryWrapper<>();
                    wrapper.eq(OrderItemPo::getCommodityId, commodityDo.getId());
//                    commodityVo.setTradeNumber(orderItemMapper.selectCount(wrapper));
                    int count = orderItemMapper.selectCount(wrapper);
                    commodityVo.setTradeNumber(count);
                    return commodityVo;
                })
                .collect(Collectors.toList());

//        records.forEach(commodityVo -> {
//            QueryWrapper<OrderItemPo> wrapper = new QueryWrapper<>();
//            wrapper.eq("commodity_id", commodityVo.getId());
//            commodityVo.setTradeNumber(orderItemMapper.selectCount(wrapper).longValue());
//        });

        return new HashMap<String, Object>() {
            {
                put("records", records);
                put("totalPages", pages.getPages());
                put("current", pages.getCurrent());
            }
        };
    }


}
