package com.fc.business.facade;

import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.OrderItemCreateVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author lzr
 * @date 2021/7/9 11:26
 */
@FeignClient(
        value = "fc-customer",
        path = "/customer/orderItem"
)
public interface OrderItemFacade {

    @PostMapping("/create")
    @ResponseBody
    Map<String, Object> create(@RequestParam("userId") Long userId, @RequestBody OrderItemCreateVo orderItemCreateVo);

    @DeleteMapping("/deleteById")
    @ResponseBody
    Map<String, Object> deleteById(@RequestParam("id") Long id);

    @PutMapping("/update")
    @ResponseBody
    Map<String, Object> update(OrderItemCreateVo orderItemCreateVo);

    @GetMapping("/selectById")
    @ResponseBody
    Map<String, Object> selectById(@RequestParam("id") Long id);
}
