package com.fc.business.facade;

import com.fc.common.domain.UserDo;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author lzr
 * @date 2021/7/13 9:33
 */
@FeignClient(
        value = "fc-customer",
        path = "/customer/user"
)
public interface UserFacade {
    @GetMapping("/selectById")
    @ResponseBody
    UserDo selectById(@RequestParam("id") Long id);
}
