package com.fc.business.controller;

import com.fc.business.service.AdminService;
import com.fc.common.convert.AdminDomainConvert;
import com.fc.common.domain.AdminDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.AdminVo;
import com.fc.common.vo.NoticeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 13:16
 */
@Api(
        tags = "管理员模块",
        value = "管理员模块"
)
@RestController
@RequestMapping("/business/admin")
public class AdminController {
    @Resource
    private AdminService adminService;

    @GetMapping("/create")
    @ApiOperation(notes = "创建管理员", value = "创建管理员")
    public Map<String, Object> create(@RequestParam("storeId") Long storeId,@RequestParam("adminId")Long adminId) {
        AdminDo adminDo = new AdminDo();
        adminDo.setStoreId(storeId);
        adminDo.setAdminId(adminId);
        adminService.create(adminDo);
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除管理员", value = "通过id删除管理员")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        adminService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "根据商店Id查询商品", value = "根据商店Id查询商品")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(adminService.selectByStoreId(storeId))
                .map();
    }
}
