package com.fc.business.controller;

import com.fc.business.service.ProblemService;
import com.fc.common.convert.ProblemDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.ProblemCreateVo;
import com.fc.common.vo.ProblemShowVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 15:09
 */
@Api(
        tags = "售后模块",
        value = "售后模块"
)
@RestController
@RequestMapping("/business/problem")
public class ProblemController {
    @Autowired
    private ProblemService problemService;

    @Autowired
    private ProblemDomainConvert problemDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建售后", value = "创建售后")
    public Map<String, Object> create(@RequestBody ProblemCreateVo problemCreateVo) {
        problemService.create(problemDomainConvert.convert(problemCreateVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除售后", value = "通过id删除售后")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        problemService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新售后", value = "更新售后")
    public Map<String, Object> update(ProblemCreateVo problemCreateVo){
        problemService.update(problemDomainConvert.convert(problemCreateVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新售后成功")
                .map();
    }

    @GetMapping("/solveProblemById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> solveProblemById(@RequestParam("id") Long id){
        problemService.solveProblemById(id);
        return ResponseDTO.successBuilder()
                .addDataValue("该售后已解决")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(problemService.selectById(id))
                .map();
    }

    @GetMapping("/selectByCustomerId")
    @ApiOperation(notes = "通过顾客id查询", value = "通过顾客id查询")
    public Map<String, Object> selectByCustomerId(@RequestParam("storeId") Long storeId, @RequestParam("customerId") Long customerId){
        return ResponseDTO.successBuilder()
                .addDataValue(problemService.selectByCustomerId(storeId, customerId))
                .map();
    }

    @GetMapping("/selectByIsSolve")
    @ApiOperation(notes = "查询未解决问题", value = "查询未解决问题")
    public Map<String, Object> selectByIsSolve(@RequestParam("storeId") Long storeId, @RequestParam("isSolve") Boolean isSolve){
        return ResponseDTO.successBuilder()
                .addDataValue(problemService.selectByIsSolve(storeId, isSolve))
                .map();
    }
}
