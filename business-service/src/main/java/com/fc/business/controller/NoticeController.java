package com.fc.business.controller;

import com.fc.business.service.NoticeService;
import com.fc.common.convert.NoticeDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.NoticeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 13:10
 */
@Api(
        tags = "B端推送模块",
        value = "B端推送模块"
)
@RestController
@RequestMapping("/business/notice")
public class NoticeController {
    @Resource
    private NoticeService noticeService;

    @Resource
    private NoticeDomainConvert noticeDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建推送", value = "创建推送")
    public Map<String, Object> create(@RequestBody NoticeVo noticeVo) {
        noticeService.create(noticeDomainConvert.convert(noticeVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除推送", value = "通过id删除推送")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        noticeService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "根据商店Id查询商品", value = "根据商店Id查询商品")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId,@RequestParam("page") Integer page,@RequestParam("size") Integer size){
        return ResponseDTO.successBuilder()
                .addDataValue(noticeService.selectByStoreId(storeId,page,size))
                .map();
    }
}
