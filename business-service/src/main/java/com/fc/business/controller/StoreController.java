package com.fc.business.controller;

import com.fc.business.service.StoreService;
import com.fc.common.convert.StoreDomainConvert;
import com.fc.common.domain.StoreDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.NoticeVo;
import com.fc.common.vo.StoreVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 14:34
 */
@Api(
        tags = "商店模块",
        value = "商店模块"
)
@RestController
@RequestMapping("/business/store")
public class StoreController {
    @Resource
    private StoreService storeService;

    @Resource
    private StoreDomainConvert storeDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建商店", value = "创建商店")
    public Map<String, Object> create(@RequestBody StoreDo storeDo) {
        storeService.create(storeDo);
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除商店", value = "通过id删除商店")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        storeService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新商店", value = "更新商店")
    public Map<String, Object> update(StoreDo storeDo){
        storeService.update(storeDo);
        return ResponseDTO.successBuilder()
                .addMessage("更新商品成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(storeService.selectById(id))
                .map();
    }

    @GetMapping("/selectByKeeperId")
    @ApiOperation(notes = "通过店主id查询", value = "通过店主id查询")
    public Map<String, Object> selectByKeeperId(@RequestParam("storekeeperId") Long storekeeperId){
        return ResponseDTO.successBuilder()
                .addDataValue(storeService.selectByKeeperId(storekeeperId))
                .map();
    }

    @GetMapping("/searchByStoreName")
    @ApiOperation(notes = "根据商店名称搜索店铺", value = "根据商店名称搜索店铺")
    public Map<String, Object> searchByStoreName(@RequestParam("storeName") String storeName,@RequestParam("page") Integer page,@RequestParam("size") Integer size){
        return ResponseDTO.successBuilder()
                .addDataValue(storeService.searchByStoreName(storeName, page, size))
                .map();
    }

    @GetMapping("/getList")
    @ApiOperation(value = "分页获取店铺")
    public Map<String, Object> getList(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){
        return ResponseDTO.successBuilder()
                .addDataValue(storeService.getList(pageNum, pageSize))
                .map();
    }
}
