package com.fc.business.controller;

import com.fc.business.service.EvaluationService;
import com.fc.common.convert.EvaluationDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.EvaluationVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 13:30
 */
@Api(
        tags = "B端评价模块",
        value = "B端评价模块"
)
@RestController
@RequestMapping("/business/evaluation")
public class EvaluationController {
    @Resource
    private EvaluationService evaluationService;

    @Resource
    private EvaluationDomainConvert evaluationDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建评价", value = "创建评价")
    public Map<String, Object> create(@RequestBody EvaluationVo evaluationVo) {
        evaluationService.create(evaluationDomainConvert.convert(evaluationVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除评价", value = "通过id删除评价")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        evaluationService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }


    @GetMapping("/selectByCommodityId")
    @ApiOperation(notes = "通过商品Id查询评论", value = "通过商品Id查询评论")
    public Map<String, Object> selectByCommodityId(@RequestParam("commodityId") Long commodityId,@RequestParam("page") Integer page,@RequestParam("size") Integer size){
        return ResponseDTO.successBuilder()
                .addDataValue(evaluationService.selectByCommodityId(commodityId,page,size))
                .map();
    }

}
