package com.fc.business.controller;

import com.fc.business.service.HomePageService;
import com.fc.common.dto.ResponseDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/2 15:12
 */
@Api(
        tags = "B端首页模块",
        value = "B端首页模块"
)
@RestController
@RequestMapping("/business/homePage")
public class HomePageController {
    @Resource
    private HomePageService homePageService;

    @GetMapping("/getOrderStatistic")
    @ApiOperation(notes = "根据商店Id查询店铺首页订单相关数据", value = "根据商店Id查询店铺首页订单相关数据")
    public Map<String, Object> getOrderStatistic(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(homePageService.getOrderStatistic(storeId))
                .map();
    }

    @GetMapping("/getStoreStatistic")
    @ApiOperation(notes = "根据商店Id查询店铺首页店铺/商品相关数据", value = "根据商店Id查询店铺首页店铺/商品相关数据")
    public Map<String, Object> getStoreStatistic(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(homePageService.getStoreStatistic(storeId))
                .map();
    }
}
