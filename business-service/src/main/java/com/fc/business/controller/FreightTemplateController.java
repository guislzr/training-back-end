package com.fc.business.controller;

import com.fc.business.service.FreightTemplateService;
import com.fc.common.convert.FreightTemplateDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.FreightTemplateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/1 14:51
 */
@Api(
        tags = "B端运费模块",
        value = "B端运费模块"
)
@RestController
@RequestMapping("/business/freightTemplate")
public class FreightTemplateController {
    @Resource
    private FreightTemplateService freightTemplateService;

    @Resource
    private FreightTemplateDomainConvert freightTemplateDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建运费模板", value = "创建运费模板")
    public Map<String, Object> create(@RequestBody FreightTemplateVo freightTemplateVo) {
        freightTemplateService.create(freightTemplateDomainConvert.convert(freightTemplateVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除", value = "通过id删除")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        freightTemplateService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新", value = "更新")
    public Map<String, Object> update(FreightTemplateVo freightTemplateVo){
        freightTemplateService.update(freightTemplateDomainConvert.convert(freightTemplateVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新商品成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(freightTemplateService.selectById(id))
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "根据商店Id查询运费模块", value = "根据商店Id查询运费模块")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(freightTemplateService.selectByStoreId(storeId))
                .map();
    }
}
