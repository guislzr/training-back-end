package com.fc.business.controller;

import com.fc.business.service.CommodityService;
import com.fc.common.constant.Type;
import com.fc.common.convert.CommodityDomainConvert;
import com.fc.common.domain.CommodityDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author lzr
 * @date 2021/3/18 15:23
 */
@Api(
        tags = "商品模块",
        value = "商品模块"
)
@RestController
@RequestMapping("/business/commodity")
public class CommodityController {

    @Resource
    private CommodityService commodityService;

    @Resource
    private CommodityDomainConvert commodityDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建商品", value = "创建商品")
    public Map<String, Object> create(@RequestBody CommodityDo commodityDo) {
        commodityService.create(commodityDo);
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除商品", value = "通过id删除商品")
    public Map<String, Object> deleteById(@RequestParam("id") Long id) {
        commodityService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新商品", value = "更新商品")
    public Map<String, Object> update(CommodityDo commodityDo) {
        commodityService.update(commodityDo);
        return ResponseDTO.successBuilder()
                .addMessage("更新商品成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityService.selectById(id))
                .map();
    }

    @GetMapping("/selectByName")
    @ApiOperation(notes = "根据商品名称查询", value = "根据商品名称查询")
    public Map<String, Object> selectByName(@RequestParam("commodityName") String commodityName) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityService.selectByName(commodityName))
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "根据商店Id查询商品", value = "根据商店Id查询商品")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityService.selectByStoreId(storeId, page, size))
                .map();
    }

    @GetMapping("/getAllType")
    @ApiOperation(notes = "查询所有商品类型", value = "查询所有商品类型")
    public Map<String, Object> getAllType() {
        Type type = new Type();
        return ResponseDTO.successBuilder()
                .addDataValue(type)
                .map();
    }
}
