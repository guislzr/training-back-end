package com.fc.business.controller;

import com.fc.business.service.AlbumService;
import com.fc.common.convert.AlbumDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.AlbumVo;
import com.fc.common.vo.CommodityVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.ws.WebEndpoint;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0
 * @date 2021/4/3 20:34
 */
@Api(
        tags = "相册模块",
        value = "相册模块"
)
@RestController
@RequestMapping("/business/album")
public class AlbumController {
    @Resource
    private AlbumService albumService;

    @Resource
    private AlbumDomainConvert albumDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建相册", value = "创建相册")
    public Map<String, Object> create(@RequestBody AlbumVo albumVo) {
        albumService.create(albumDomainConvert.convert(albumVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除相册", value = "通过id删除相册")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        albumService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @GetMapping("/addPicture")
    @ApiOperation(notes = "通过相册id添加照片", value = "通过相册id添加照片")
    public Map<String, Object> addPicture(@RequestParam("id")Long id, @RequestParam("picture")String picture){
        albumService.addPicture(id, picture);
        return ResponseDTO.successBuilder()
                .addMessage("添加照片成功")
                .map();
    }

    @GetMapping("/deletePicture")
    @ApiOperation(notes = "通过相册id和照片序号删除照片", value = "通过相册id和照片序号删除照片")
    public Map<String, Object> deletePicture(@RequestParam("id")Long id, @RequestParam("pictureOrder")Integer pictureOrder){
        albumService.deletePicture(id, pictureOrder);
        return ResponseDTO.successBuilder()
                .addMessage("删除照片成功")
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "通过商店id查找所有相册", value = "通过商店id查找所有相册")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(albumService.selectByStoreId(storeId))
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过相册id查找", value = "通过相册id查找")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(albumService.selectById(id))
                .map();
    }
}
