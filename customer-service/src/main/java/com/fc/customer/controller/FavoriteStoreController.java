package com.fc.customer.controller;

import com.fc.common.convert.FavoriteStoreDomainConvert;
import com.fc.common.domain.FavoriteStoreDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.FavoriteStoreVo;
import com.fc.customer.service.FavoriteStoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:38
 */
@Api(
        tags = "关注店铺模块",
        value = "关注店铺模块"
)
@RestController
@RequestMapping("/customer/favoriteStore")
public class FavoriteStoreController {

    @Autowired
    private FavoriteStoreService favoriteStoreService;

    @Autowired
    private FavoriteStoreDomainConvert favoriteStoreDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "添加关注店铺", value = "添加关注店铺")
    public Map<String, Object> create(@RequestParam FavoriteStoreDo favoriteStoreDo){
        return ResponseDTO.successBuilder()
                .addDataValue(favoriteStoreService.create(favoriteStoreDo))
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "根据id取关店铺", value = "根据id取关店铺")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        favoriteStoreService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新关注的店铺", value = "更新关注的店铺")
    public Map<String, Object> update(FavoriteStoreVo favoriteStoreVo){
        favoriteStoreService.update(favoriteStoreDomainConvert.convert(favoriteStoreVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新店铺关注成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(favoriteStoreService.selectById(id))
                .map();
    }

    @GetMapping("/selectByUserId")
    @ApiOperation(notes = "通过用户id查询他关注的店铺", value = "通过用户id查询他关注的店铺")
    public Map<String, Object> selectByUserId(@RequestParam("userId") Long userId) throws ParseException {
        return ResponseDTO.successBuilder()
                .addDataValue(favoriteStoreService.selectByUserId(userId))
                .map();
    }
}
