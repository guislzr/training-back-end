package com.fc.customer.controller;

import com.fc.common.convert.CommodityCartDomainConvert;
import com.fc.common.convert.HomeBannerDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityCartVo;
import com.fc.common.vo.HomeBannerVo;
import com.fc.customer.service.CommodityCartService;
import com.fc.customer.service.HomeBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:49
 */
@Api(
        tags = "首页滚动图",
        value = "首页滚动图"
)
@RestController
@RequestMapping("/customer/homeBanner")
public class HomeBannerController {
    @Resource
    private HomeBannerService homeBannerService;

    @Resource
    private HomeBannerDomainConvert homeBannerDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建首页滚动图", value = "创建首页滚动图")
    public Map<String, Object> create(@RequestBody HomeBannerVo homeBannerVo) {
        homeBannerService.create(homeBannerDomainConvert.convert(homeBannerVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除", value = "通过id删除")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        homeBannerService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新首页滚动图", value = "更新首页滚动图")
    public Map<String, Object> update(HomeBannerVo homeBannerVo){
        homeBannerService.update(homeBannerDomainConvert.convert(homeBannerVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(homeBannerService.selectById(id))
                .map();
    }

    @GetMapping("/showAll")
    @ApiOperation(notes = "展示所有banner", value = "展示所有banner")
    public Map<String, Object> showAll(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){
        return ResponseDTO.successBuilder()
                .addDataValue(homeBannerService.show(pageNum, pageSize))
                .map();
    }

}
