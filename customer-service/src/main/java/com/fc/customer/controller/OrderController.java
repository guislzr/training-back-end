package com.fc.customer.controller;

import com.fc.common.context.AdminSecurityContextHolder;
import com.fc.common.convert.OrderDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.po.status.OrderStatus;
import com.fc.common.vo.OrderCreateVo;
import com.fc.common.vo.OrderItemCreateVo;
import com.fc.customer.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:50
 */
@Api(
        tags = "订单模块",
        value = "订单模块"
)
@RestController
@RequestMapping("/customer/order")
public class OrderController {
    @Resource
    private OrderService orderService;

    @Resource
    private OrderDomainConvert orderDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建订单", value = "创建订单")
    public Map<String, Object> create(@RequestBody OrderCreateVo orderCreateVo) {
        orderService.create(orderCreateVo, orderCreateVo.getOrderItemCreateVos());
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除订单", value = "通过id删除订单")
    public Map<String, Object> deleteById(@RequestParam("id") Long id) {
        orderService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新订单", value = "更新订单")
    public Map<String, Object> update(@RequestBody OrderCreateVo orderVo) {
        orderService.update(orderDomainConvert.convert(orderVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id) {
        return ResponseDTO.successBuilder()
                .addDataValue(orderService.selectById(id))
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "通过店铺id查询相应状态的订单", value = "通过店铺id查询相应状态的订单")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId, @RequestParam("orderStatus") OrderStatus orderStatus, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return ResponseDTO.successBuilder()
                .addDataValue(orderService.selectByStoreId(storeId, orderStatus, page, size))
                .map();
    }

    @GetMapping("/getByUserId")
    @ApiOperation(value = "分页获取用户订单")
    public Map<String, Object> getByUserId(@RequestParam("userId") Long userId, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize) {
        return ResponseDTO.successBuilder()
                .addDataValue(orderService.getByUserId(userId, pageNum, pageSize))
                .map();
    }

}
