package com.fc.customer.controller;

import com.fc.common.convert.OrderItemDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityVo;
import com.fc.common.vo.OrderItemCreateVo;
import com.fc.common.vo.OrderItemShowVo;
import com.fc.customer.service.CommodityListService;
import com.fc.customer.service.OrderItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 13:57
 */
@Api(
        tags = "订单项模块",
        value = "订单项模块"
)
@RestController
@RequestMapping("/customer/orderItem")
public class OrderItemController {
    @Resource
    private OrderItemService orderItemService;

    @Autowired
    private CommodityListService commodityListService;

    @Resource
    private OrderItemDomainConvert orderItemDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建订单项", value = "创建订单项")
    public Map<String, Object> create(@RequestParam("userId") Long userId, @RequestBody OrderItemCreateVo orderItemCreateVo) {
        orderItemService.create(userId, orderItemCreateVo);
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除订单项", value = "通过id删除订单项")
    public Map<String, Object> deleteById(@RequestParam("id") Long id) {
        orderItemService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新订单项", value = "更新订单项")
    public Map<String, Object> update(OrderItemCreateVo orderItemCreateVo) {
        orderItemService.update(orderItemDomainConvert.convert(orderItemCreateVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询项", value = "通过id查询项")
    public Map<String, Object> selectById(@RequestParam("id") Long id) {
        return ResponseDTO.successBuilder()
                .addDataValue(orderItemService.selectById(id))
                .map();
    }

    @GetMapping("/getByIds")
    @ApiOperation(value = "通过id批量获取")
    public Map<String, Object> getByIds(@RequestParam("ids") List<Long> ids) {
        List<OrderItemShowVo> list = orderItemService.getByIds(ids).stream().map(orderItemDomainConvert::convert).collect(Collectors.toList());
        list.forEach(orderItemShowVo -> {
            CommodityVo commodityDetail = commodityListService.getCommodityDetail(orderItemShowVo.getCommodityId());
            orderItemShowVo.setPayment(commodityDetail.getRentPrice() * orderItemShowVo.getNumber() * orderItemShowVo.getRentDays());
        });
        return ResponseDTO.successBuilder()
                .addDataValue(list)
                .map();
    }
}
