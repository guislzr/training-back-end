package com.fc.customer.controller;

import com.fc.common.convert.CommodityDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.customer.service.CommodityListService;
import com.fc.customer.vo.SortVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/10 15:01
 */
@Api(
        tags = "商品显示",
        value = "商品显示"
)
@RestController
@RequestMapping("/customer/commodityList")
public class CommodityListController {
    @Resource
    private CommodityListService commodityListService;

    @Resource
    private CommodityDomainConvert commodityDomainConvert;


    @GetMapping("/showCommodityList")
    @ApiOperation(notes = "按照时间先后查询商品列表", value = "按照时间先后查询商品列表")
    public Map<String, Object> showCommodityList(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.showCommodityList(pageNum, pageSize))
                .map();
    }


    @GetMapping("/searchCommodityList")
    @ApiOperation(notes = "按照类型查询商品列表", value = "按照类型查询商品列表")
    public Map<String, Object> searchCommodityList(@RequestParam("type") String type, @RequestParam("pageNum") Integer pageNum,
                                                   @RequestParam("pageSize") Integer pageSize,
                                                   SortVo sortVo) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.searchCommodityList(type, pageNum, pageSize, sortVo))
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过商品id查询", value = "通过商品id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id) {
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.getCommodityDetail(id))
                .map();
    }

    @GetMapping("/showHot")
    @ApiOperation(value = "获取销量高的商品")
    public Map<String, Object> showHot(){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.showHot().stream().map(commodityDomainConvert::convert).collect(Collectors.toList()))
                .map();
    }

    @GetMapping("/getRecord")
    @ApiOperation(value = "获取浏览商品")
    public Map<String, Object> getRecord(@RequestParam("userId") Long userId){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.getRecord(userId).stream().map(commodityDomainConvert::convert).collect(Collectors.toList()))
                .map();
    }

    @GetMapping("/searchByName")
    @ApiOperation(value = "根据名称搜索")
    public Map<String, Object> searchByName(@RequestParam("name") String name,
                                            @RequestParam("pageNum") Integer pageNum,
                                            @RequestParam("pageSize") Integer pageSize,
                                            SortVo sortVo){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.searchByName(name, pageNum, pageSize, sortVo))
                .map();
    }

    @GetMapping("/recommend")
    @ApiOperation(value = "获取推荐商品")
    public Map<String, Object> recommend(@RequestParam("userId") Long userId){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityListService.getRecommend(userId))
                .map();
    }

}
