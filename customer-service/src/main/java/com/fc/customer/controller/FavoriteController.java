package com.fc.customer.controller;

import com.fc.common.convert.FavoriteDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.FavoriteVo;
import com.fc.customer.service.FavoriteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javafx.scene.shape.SVGPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 13:40
 */
@RestController
@RequestMapping("/customer/favorite")
@Api(
        tags = "商品收藏模块",
        value = "商品收藏模块"
)
public class FavoriteController {

    @Autowired
    private FavoriteService favoriteService;

    @Autowired
    private FavoriteDomainConvert favoriteDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "添加商品收藏", value = "添加商品收藏")
    public Map<String, Object> create(@RequestBody FavoriteVo favoriteVo){
        return ResponseDTO.successBuilder()
                .addDataValue(favoriteService.create(favoriteDomainConvert.convert(favoriteVo)))
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除收藏", value = "通过id删除收藏")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        favoriteService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新收藏", value = "更新收藏")
    public Map<String, Object> update(FavoriteVo favoriteVo){
        favoriteService.update(favoriteDomainConvert.convert(favoriteVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新收藏成功")
                .map();
    }

    @GetMapping("/selectByUserId")
    @ApiOperation(notes = "根据用户id查询用户的收藏", value = "根据用户id查询用户的收藏")
    public Map<String, Object> selectByUserId(@RequestParam("userId") Long id){
        return  ResponseDTO.successBuilder()
                .addDataValue(favoriteService.selectByUserId(id))
                .map();
    }
}
