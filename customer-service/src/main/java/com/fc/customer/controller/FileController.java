package com.fc.customer.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.swagger.annotations.ApiOperation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @author lzr
 * @date 2021/7/12 8:36
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @PostMapping(value = "/upload", headers = "content-type=multipart/form-data")
    @Transactional(
            rollbackFor = Exception.class
    )
    @ApiOperation(
            value = "上传文件",
            notes = "上传文件"
    )
    public JsonNode uploadImages(@RequestParam(value = "file") MultipartFile file) {
        ObjectNode result = new ObjectMapper().createObjectNode();
        if (file.isEmpty()) {
            result.put("msg","文件不存在");
            return result;
        }

        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        String filePath = "//usr//temp//image//"; // 上传后的路径,即本地磁盘
        fileName = UUID.randomUUID() + suffixName; // 新文件名
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String filename = "http://47.110.156.20:8081/images/" + fileName;//本地目录和生成的文件名拼接，这一段存入数据库

        result.put("code",0);
        result.put("msg","上传成功");
        result.put("imgUrl",filename);
        return result;
    }
}
