package com.fc.customer.controller;

import com.fc.common.convert.UserDomainConvert;
import com.fc.common.domain.UserDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.po.UserPo;
import com.fc.common.vo.UserCreateVo;
import com.fc.common.vo.UserVo;
import com.fc.customer.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/9 18:33
 */
@Api(
        tags = "登陆注册",
        value = "登陆注册"
)
@RestController
@RequestMapping("/customer/user")
public class UserController {
    @Resource
    private UserService userService;

    @Resource
    private UserDomainConvert userDomainConvert;

    @PostMapping("/singUp")
    @ApiOperation(notes = "注册", value = "注册")
    public Map<String, Object> signUp(@Validated @RequestBody UserCreateVo userCreateVo) {
        UserPo userPo = new UserPo();
        BeanUtils.copyProperties(userCreateVo, userPo);
        Long id = userService.create(userPo);
        return ResponseDTO.successBuilder()
                .addData("用户id", id)
                .addMessage("创建成功")
                .map();
    }

    @PostMapping("/login")
    @ApiOperation(notes = "登录", value = "登录")
    public Map<String, Object> login(@Validated @RequestBody UserCreateVo userCreateVo, HttpServletResponse response) {
        UserPo userPo = new UserPo();
        BeanUtils.copyProperties(userCreateVo, userPo);
        Long id = userService.login(userPo);
        Cookie cookie = new Cookie("userId", String.valueOf(id));
        cookie.setMaxAge(60 * 3600);
        cookie.setPath("/");
        response.addCookie(cookie);

        return ResponseDTO.successBuilder()
                .addData("用户id", id)
                .addMessage("登陆成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除用户", value = "通过id删除用户")
    public Map<String, Object> deleteById(@RequestParam("id") Long id) {
        userService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新用户", value = "更新用户")
    public Map<String, Object> update(@RequestBody UserVo userVo) {
        UserPo userPo = new UserPo();
        BeanUtils.copyProperties(userVo, userPo);
        userService.update(userPo);
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public UserDo selectById(@RequestParam("id") Long id) {
        return userService.selectById(id);
    }
}
