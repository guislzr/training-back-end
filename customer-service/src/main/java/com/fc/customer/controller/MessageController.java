package com.fc.customer.controller;

import com.fc.common.convert.CommodityCartDomainConvert;
import com.fc.common.convert.MessageDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityCartVo;
import com.fc.common.vo.MessageVo;
import com.fc.customer.service.CommodityCartService;
import com.fc.customer.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:50
 */
@Api(
        tags = "消息通知模块",
        value = "消息通知模块"
)
@RestController
@RequestMapping("/customer/message")
public class MessageController {
    @Resource
    private MessageService messageService;

    @Resource
    private MessageDomainConvert messageDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建消息通知", value = "创建消息通知")
    public Map<String, Object> create(@RequestBody MessageVo messageVo) {
        messageService.create(messageDomainConvert.convert(messageVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除消息通知", value = "通过id删除消息通知")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        messageService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新消息通知", value = "更新消息通知")
    public Map<String, Object> update(MessageVo messageVo){
        messageService.update(messageDomainConvert.convert(messageVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(messageService.selectById(id))
                .map();
    }

}
