package com.fc.customer.controller;

import com.fc.common.convert.ReportDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.ReportVo;
import com.fc.customer.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/21 16:38
 */
@Api(
        tags = "举报模块",
        value = "举报模块"
)
@RestController
@RequestMapping("/customer/report")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @Autowired
    private ReportDomainConvert reportDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建举报", value = "创建举报")
    public Map<String, Object> create(@RequestBody ReportVo reportVo){
        reportService.create(reportDomainConvert.convert(reportVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "根据id删除举报", value = "根据id删除举报")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        reportService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新举报内容", value = "更新举报内容")
    public Map<String, Object> update(ReportVo reportVo){
        reportService.update(reportDomainConvert.convert(reportVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新商品成功")
                .map();
    }

    @GetMapping("selectById")
    @ApiOperation(notes = "通过id查询举报信息", value = "通过id查询举报信息")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(reportService.selectById(id))
                .map();
    }

    @GetMapping("/selectByUserId")
    @ApiOperation(notes = "获取用户所有举报项", value = "获取用户所有举报项")
    public Map<String, Object> selectByUserId(@RequestParam("userId") Long userId){
        return ResponseDTO.successBuilder()
                .addDataValue(reportService.selectByUserId(userId))
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "获取店铺所有举报", value = "获取店铺所有举报")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(reportService.selectByStoreId(storeId))
                .map();
    }

    @GetMapping("/selectByCommodityId")
    @ApiOperation(notes = "获取商品的所有举报", value = "获取商品的所有举报")
    public Map<String, Object> selectByCommodityId(@RequestParam("commodityId") Long commodityId){
        return ResponseDTO.successBuilder()
                .addDataValue(reportService.selectByCommodityId(commodityId))
                .map();
    }
}
