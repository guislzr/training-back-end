package com.fc.customer.controller;

import com.fc.common.context.AdminSecurityContextHolder;
import com.fc.common.convert.CommodityCartDomainConvert;
import com.fc.common.convert.CommodityDomainConvert;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityCartVo;
import com.fc.common.vo.CommodityVo;
import com.fc.customer.service.CommodityCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:34
 */
@Api(
        tags = "购物车模块",
        value = "购物车模块"
)
@RestController
@RequestMapping("/customer/commodityCart")
public class CommodityCartController {
    @Resource
    private CommodityCartService commodityCartService;

    @Resource
    private CommodityCartDomainConvert commodityCartDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建购物车记录", value = "创建购物车记录")
    public Map<String, Object> create(@RequestBody CommodityCartDo commodityCartDo) {
        commodityCartService.create(commodityCartDo);
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除购物车记录", value = "通过id删除购物车记录")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        commodityCartService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新购物车记录", value = "更新购物车记录")
    public Map<String, Object> update(CommodityCartDo commodityCartDo){
        commodityCartService.update(commodityCartDo);
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityCartService.selectById(id))
                .map();
    }

    @GetMapping("/getByUserId")
    @ApiOperation(value = "获取用户购物车")
    public Map<String, Object> getByUserId(@RequestParam("userId") Long userId, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize){
        return ResponseDTO.successBuilder()
                .addDataValue(commodityCartService.getByUserId(userId, pageNum, pageSize))
                .map();
    }

    @DeleteMapping("/deleteByUserId")
    @ApiOperation(value = "根据userId清空购物车")
    public Map<String, Object> deleteByUserId(@RequestParam("userId") Long userId){
        commodityCartService.deleteByUserId(userId);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }
}
