package com.fc.customer.controller;

import com.fc.common.convert.HomeTypeDomainConvert;
import com.fc.common.domain.HomeTypeDo;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.HomeTypeVo;
import java.util.*;
import com.fc.customer.service.HomeTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:50
 */
@Api(
        tags = "首页商品分类模块",
        value = "首页商品分类模块"
)
@RestController
@RequestMapping("/customer/homeType")
public class HomeTypeController {
    @Resource
    private HomeTypeService homeTypeService;

    @Resource
    private HomeTypeDomainConvert homeTypeDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建首页商品分类", value = "创建首页商品分类")
    public Map<String, Object> create(@RequestBody HomeTypeVo homeTypeVo) {
        homeTypeService.create(homeTypeDomainConvert.convert(homeTypeVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @PostMapping("/saveBatch")
    @ApiOperation(value = "批量创建")
    public Map<String, Object> saveBatch(@RequestBody List<HomeTypeVo> homeTypeVos){
        List<HomeTypeDo> homeTypeDos = homeTypeVos.stream().map(homeTypeDomainConvert::convert).collect(Collectors.toList());
        return ResponseDTO.successBuilder()
                .addDataValue(homeTypeService.saveAll(homeTypeDos))
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除首页商品分类", value = "通过id删除首页商品分类")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        homeTypeService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新首页商品分类", value = "更新首页商品分类")
    public Map<String, Object> update(HomeTypeVo homeTypeVo){
        homeTypeService.update(homeTypeDomainConvert.convert(homeTypeVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(homeTypeService.selectById(id))
                .map();
    }

    @GetMapping("/showAll")
    @ApiOperation(notes = "展示所有type按钮", value = "展示所有type按钮")
    public Map<String, Object> showAll(){
        return ResponseDTO.successBuilder()
                .addDataValue(homeTypeService.showAll())
                .map();
    }

}
