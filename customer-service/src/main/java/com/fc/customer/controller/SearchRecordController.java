package com.fc.customer.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.po.SearchRecordPo;
import com.fc.customer.service.SearchRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author lzr
 * @date 2021/7/10 14:19
 */
@Api(
        tags = "浏览记录",
        value = "浏览记录"
)
@RestController
@RequestMapping("/customer/searchRecord")
public class SearchRecordController {

    @Autowired
    private SearchRecordService searchRecordService;

    @GetMapping("/getById")
    @ApiOperation(value = "通过id获取")
    public Map<String, Object> getById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(searchRecordService.getById(id))
                .map();
    }

    @GetMapping("/getByUserId")
    @ApiOperation(value = "通过id获取")
    public Map<String, Object> getByUserId(@RequestParam("userId") Long userId, @RequestParam("pageNum") Integer pageNum,
                                           @RequestParam("pageSize") Integer pageSize){
        return ResponseDTO.successBuilder()
                .addDataValue(searchRecordService.getByUserId(userId, pageNum, pageSize))
                .map();
    }

    @PostMapping("/create")
    @ApiOperation(value = "创建搜索记录")
    public Map<String, Object> create(@RequestBody SearchRecordPo searchRecordPo){
        boolean flag = false;
        SearchRecordPo one = searchRecordService.lambdaQuery().eq(SearchRecordPo::getCommodityId, searchRecordPo.getCommodityId())
                .eq(SearchRecordPo::getUserId, searchRecordPo.getUserId()).one();
        if(Objects.nonNull(one)){
            one.setGmtCreate(new Date());
            flag = searchRecordService.updateById(one);
        }else{
            flag = searchRecordService.save(searchRecordPo);
        }
        return ResponseDTO.successBuilder()
                .addDataValue(flag)
                .map();
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "通过id删除")
    public Map<String, Object> delete(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(searchRecordService.removeById(id))
                .map();
    }

    @DeleteMapping("/deleteByUserId")
    @ApiOperation(value = "通过用户id删除")
    public Map<String, Object> deleteByUserId(@RequestParam("userId") Long userId){
        LambdaQueryWrapper<SearchRecordPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SearchRecordPo::getUserId, userId);
        return ResponseDTO.successBuilder()
                .addDataValue(searchRecordService.remove(lambdaQueryWrapper))
                .map();
    }

    @DeleteMapping("/deleteBatch")
    @ApiOperation(value = "批量删除")
    public Map<String, Object> deleteBatch(@RequestBody List<Long> ids){
        return ResponseDTO.successBuilder()
                .addDataValue(searchRecordService.removeByIds(ids))
                .map();
    }
}
