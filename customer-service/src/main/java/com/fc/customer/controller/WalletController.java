package com.fc.customer.controller;

import com.fc.common.convert.CommodityCartDomainConvert;
import com.fc.common.convert.WalletDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.CommodityCartVo;
import com.fc.common.vo.WalletVo;
import com.fc.customer.service.CommodityCartService;
import com.fc.customer.service.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:50
 */
@Api(
        tags = "钱包模块",
        value = "钱包模块"
)
@RestController
@RequestMapping("/customer/wallet")
public class WalletController {
    @Resource
    private WalletService walletService;

    @Resource
    private WalletDomainConvert walletDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建钱包", value = "创建钱包")
    public Map<String, Object> create(@RequestBody WalletVo walletVo) {
        walletService.create(walletDomainConvert.convert(walletVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除钱包", value = "通过id删除钱包")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        walletService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新钱包", value = "更新钱包")
    public Map<String, Object> update(WalletVo walletVo){
        walletService.update(walletDomainConvert.convert(walletVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(walletService.selectById(id))
                .map();
    }

}
