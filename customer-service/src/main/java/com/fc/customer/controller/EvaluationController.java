package com.fc.customer.controller;

import com.fc.common.convert.EvaluationDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.EvaluationVo;
import com.fc.customer.service.EvaluationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:49
 */
@Api(
        tags = "评价模块",
        value = "评价模块"
)
@RestController
@RequestMapping("/customer/evaluation")
public class EvaluationController {
    @Resource
    private EvaluationService evaluationService;

    @Resource
    private EvaluationDomainConvert evaluationDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建评价", value = "创建评价")
    public Map<String, Object> create(@RequestBody EvaluationVo evaluationVo) {
        evaluationService.create(evaluationDomainConvert.convert(evaluationVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除评价", value = "通过id删除评价")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        evaluationService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新评价", value = "更新评价")
    public Map<String, Object> update(EvaluationVo evaluationVo){
        evaluationService.update(evaluationDomainConvert.convert(evaluationVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询", value = "通过id查询")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(evaluationService.selectById(id))
                .map();
    }

}
