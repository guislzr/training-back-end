package com.fc.customer.controller;

import com.fc.common.context.AdminSecurityContextHolder;
import com.fc.common.convert.DiscountDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.DiscountVo;
import com.fc.customer.service.DiscountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 11:21
 */
@Api(
        tags = "优惠券模块",
        value = "优惠券模块"
)
@RestController
@RequestMapping("/customer/discount")
public class DiscountController {

    @Autowired
    private DiscountService discountService;

    @Autowired
    private DiscountDomainConvert discountDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建优惠券", value = "创建优惠券")
    public Map<String, Object> create(@RequestBody DiscountVo discountVo){
        discountService.create(discountDomainConvert.convert(discountVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "通过id删除优惠券", value = "通过id删除优惠券")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        discountService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新优惠券", value = "更新优惠券")
    public Map<String, Object> update(DiscountVo discountVo){
        discountService.update(discountDomainConvert.convert(discountVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新优惠券成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询优惠券", value = "通过id查询优惠券")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(discountService.selectById(id))
                .map();
    }

    @GetMapping("/selectAvailableDiscount")
    @ApiOperation(notes = "获取所有可用的优惠券", value = "获取所有可用的优惠券")
    public Map<String, Object> selectAvailableDiscount(@RequestParam("storeId") Long storeId,
                                                       @RequestParam("amount") Double amount){
        return ResponseDTO.successBuilder()
                .addDataValue(discountService.selectAvailableDiscount(AdminSecurityContextHolder.getAdminId(), storeId, amount))
                .map();
    }

    @GetMapping("/selectByUserId")
    @ApiOperation(notes = "根据用户id查询优惠券", value = "根据用户id查询优惠券")
    public Map<String, Object> selectByUserId(){
        return ResponseDTO.successBuilder()
                .addDataValue(discountService.selectByUserId(AdminSecurityContextHolder.getAdminId()))
                .map();
    }

    @GetMapping("/selectByStoreId")
    @ApiOperation(notes = "根据商家id查询优惠券", value = "根据商家id查询优惠券")
    public Map<String, Object> selectByStoreId(@RequestParam("storeId") Long storeId){
        return ResponseDTO.successBuilder()
                .addDataValue(discountService.selectByStoreId(storeId))
                .map();
    }
}
