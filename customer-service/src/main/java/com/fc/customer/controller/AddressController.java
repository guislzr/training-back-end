package com.fc.customer.controller;

import com.fc.common.convert.AddressDomainConvert;
import com.fc.common.dto.ResponseDTO;
import com.fc.common.vo.AddressVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.fc.customer.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 14:32
 */
@RestController
@RequestMapping("/customer/address")
@Api(
        tags = "收货地址",
        value = "收货地址"
)
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressDomainConvert addressDomainConvert;

    @PostMapping("/create")
    @ApiOperation(notes = "创建收货地址", value = "创建收货地址")
    public Map<String, Object> create(@RequestBody AddressVo addressVo){
        addressService.create(addressDomainConvert.convert(addressVo));
        return ResponseDTO.successBuilder()
                .addMessage("创建成功")
                .map();
    }

    @DeleteMapping("/deleteById")
    @ApiOperation(notes = "删除收货地址", value = "删除收货地址")
    public Map<String, Object> deleteById(@RequestParam("id") Long id){
        addressService.deleteById(id);
        return ResponseDTO.successBuilder()
                .addMessage("删除成功")
                .map();
    }

    @PutMapping("/update")
    @ApiOperation(notes = "更新收货地址", value = "更新收货地址")
    public Map<String, Object> update(AddressVo addressVo){
        addressService.update(addressDomainConvert.convert(addressVo));
        return ResponseDTO.successBuilder()
                .addMessage("更新地址成功")
                .map();
    }

    @GetMapping("/selectById")
    @ApiOperation(notes = "通过id查询收货地址", value = "通过id查询收货地址")
    public Map<String, Object> selectById(@RequestParam("id") Long id){
        return ResponseDTO.successBuilder()
                .addDataValue(addressService.selectById(id))
                .map();
    }

    @GetMapping("/selectByUserId")
    @ApiOperation(notes = "查询用户收货地址", value = "查询用户收货地址")
    public Map<String, Object> selectByUserId(@RequestParam("userId") Long userId){
        return ResponseDTO.successBuilder()
                .addDataValue(addressService.selectByUserId(userId))
                .map();
    }

    @GetMapping("/selectDefaultAddress")
    @ApiOperation(notes = "查询用户默认收货地址", value = "查询用户默认收货地址")
    public Map<String, Object> selectDefaultAddress(@RequestParam("userId") Long userId){
        return ResponseDTO.successBuilder()
                .addDataValue(addressService.selectDefaultAddress(userId))
                .map();
    }

}
