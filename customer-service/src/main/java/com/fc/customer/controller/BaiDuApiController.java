package com.fc.customer.controller;

import com.fc.customer.service.impl.ApplyAi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaiDuApiController {
    @Autowired
    ApplyAi applyAi;

    @CrossOrigin
    @RequestMapping("/api/baidu/{filepath}")
    public String searchResult(@PathVariable("filepath") String filepath) {
        String path = filepath.replace("*", "/");
        return applyAi.animal(path);
    }
}
