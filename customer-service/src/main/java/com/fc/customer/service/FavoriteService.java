package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.FavoriteDo;
import com.fc.common.po.FavoritePo;
import com.fc.common.vo.FavoriteVo;

import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 13:08
 */
public interface FavoriteService extends IService<FavoritePo> {

    /**
     * 添加收藏
     * @param favoriteDo
     */
    Boolean create(FavoriteDo favoriteDo);

    /**
     * 通过id取消收藏
     * @param id
     */
    void deleteById(Long id);

    /**
     * 更新收藏信息
     * @param favoriteDo
     */
    void update(FavoriteDo favoriteDo);

    FavoriteDo selectById(Long id);

    /**
     * 获取用户的所有收藏
     * @param id
     * @return
     */
    List<FavoriteVo> selectByUserId(Long id);
}
