package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.MessageDo;
import com.fc.common.domain.OrderDo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.OrderStatus;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.fc.common.vo.OrderCreateVo;
import com.fc.common.vo.OrderItemCreateVo;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:02
 */
public interface OrderService extends IService<OrderPo> {
    /**
     * 创建新订单
     * @param orderCreateVo 订单vo
     * @param orderItemCreateVos 订单项列表
     */
    void create(OrderCreateVo orderCreateVo, List<OrderItemCreateVo> orderItemCreateVos);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param orderDo 订单do
     */
    void update(OrderDo orderDo);

    OrderDo selectById(Long id);

    /**
     * 根据商店id查找全部订单
     *
     * @param storeId 商店id
     * @param orderStatus 订单状态（为null则查找全部订单）
     * @return 订单列表
     */
    Map<String, Object> selectByStoreId(Long storeId, OrderStatus orderStatus,Integer page, Integer size);

    void updatePaidOrder(Long orderId);

    Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize);
}
