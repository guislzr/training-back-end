package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.common.convert.ReportDataConvert;
import com.fc.common.domain.ReportDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.ReportMapper;
import com.fc.common.po.ReportPo;
import com.fc.customer.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @auther: 李梓睿
 * @date: 2021/3/20 11:48
 */
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportMapper reportMapper;

    @Autowired
    ReportDataConvert reportDataConvert;

    @Override
    public void create(ReportDo reportDo) {
        ReportPo reportPo = reportDataConvert.convert(reportDo);
        reportMapper.insert(reportPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = reportMapper.deleteById(id);
        if(i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(ReportDo reportDo) {
        ReportPo convert = reportDataConvert.convert(reportDo);
        convert.setId(reportDo.getId());
        int update = reportMapper.updateById(convert);
        if(update <= 0){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public ReportDo selectById(Long id) {
        ReportPo reportPo = reportMapper.selectById(id);
        return reportDataConvert.convert(reportPo);
    }

    @Override
    public List<ReportDo> selectByStoreId(Long storeId) {
        QueryWrapper<ReportPo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId);
        List<ReportPo> reportPoList = reportMapper.selectList(wrapper);
        return reportPoList.stream().map(reportPo -> reportDataConvert.convert(reportPo))
                .collect(Collectors.toList());
        //使用map操作可以遍历集合中的每个对象，并对其进行操作
        //map之后，用.collect(Collectors.toList())会得到操作后的集合。
    }

    @Override
    public List<ReportDo> selectByUserId(Long userId) {
        QueryWrapper<ReportPo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<ReportPo> reportPoList = reportMapper.selectList(wrapper);
        return reportPoList.stream().map(reportPo -> reportDataConvert.convert(reportPo))
                .collect(Collectors.toList());
    }

    @Override
    public List<ReportDo> selectByCommodityId(Long commodityId) {
        QueryWrapper<ReportPo> wrapper = new QueryWrapper<>();
        wrapper.eq("commodity_id", commodityId);
        List<ReportPo> reportPoList = reportMapper.selectList(wrapper);
        return reportPoList.stream().map(reportPo -> reportDataConvert.convert(reportPo))
                .collect(Collectors.toList());
    }
}
