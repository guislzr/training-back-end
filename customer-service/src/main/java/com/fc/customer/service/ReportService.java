package com.fc.customer.service;

import com.fc.common.domain.ReportDo;

import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/20 11:37
 */
public interface ReportService {

    /**
     * 创建对某商家某商品的举报
     * @param reportDo 举报do
     */
    void create(ReportDo reportDo);

    /**
     * 通过id删除（取消）举报
     * @param id
     */
    void deleteById(Long id);

    /**
     * 更改举报内容
     * @param reportDo
     */
    void update(ReportDo reportDo);

    /**
     * 根据举报id获取
     * @param id
     * @return
     */
    ReportDo selectById(Long id);

    /**
     * 获取店铺所有被举报
     * @param storeId
     * @return
     */
    List<ReportDo> selectByStoreId(Long storeId);

    /**
     * 获取用户所有举报
     * @param userId
     * @return
     */
    List<ReportDo> selectByUserId(Long userId);

    /**
     * 获取商品所有被举报
     * @param commodityId
     * @return
     */
    List<ReportDo> selectByCommodityId(Long commodityId);
}
