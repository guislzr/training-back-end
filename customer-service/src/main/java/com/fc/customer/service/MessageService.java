package com.fc.customer.service;

import com.fc.common.domain.HomeTypeDo;
import com.fc.common.domain.MessageDo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 11:02
 */
public interface MessageService {
    /**
     * 创建新通知
     * @param messageDo 通知的do
     */
    void create(MessageDo messageDo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param messageDo 通知的do
     */
    void update(MessageDo messageDo);

    MessageDo selectById(Long id);
}
