package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.po.SearchRecordPo;
import java.util.*;

/**
 * @author lzr
 * @date 2021/7/10 14:19
 */
public interface SearchRecordService extends IService<SearchRecordPo> {

    Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize);
}
