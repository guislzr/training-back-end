package com.fc.customer.service;

import com.fc.common.domain.DiscountDo;
import com.fc.common.mapper.DiscountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/22 22:45
 */
public interface DiscountService {

    /**
     * 为用户生成一张优惠券
     * @param discountDo
     */
    void create(DiscountDo discountDo);

    /**
     * 通过id删除优惠券（使用优惠券）
     * @param id
     */
    void deleteById(Long id);

    /**
     * 更新优惠券信息
     * @param discountDo
     */
    void update(DiscountDo discountDo);

    DiscountDo selectById(Long id);

    List<DiscountDo> selectByUserId(Long userId);

    List<DiscountDo> selectByStoreId(Long storeId);

    /**
     * 根据userId，storeId，总金额——>获取所有可用的优惠券
     * 默认将优惠金额最大的一张放到index=0的位置
     * @param userId
     * @param storeId
     * @param amount
     * @return
     */
    List<DiscountDo> selectAvailableDiscount(Long userId, Long storeId, Double amount);
}
