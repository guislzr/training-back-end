package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.po.CommodityCartPo;

import java.util.*;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:06
 */
public interface CommodityCartService extends IService<CommodityCartPo> {
    /**
     * 创建一个购物车记录
     * @param commodityCartDo 购物车记录do
     */
    void create(CommodityCartDo commodityCartDo);

    /**
     * 通过id删除购物车记录
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新购物车
     * @param commodityCartDo 购物车记录do
     */
    void update(CommodityCartDo commodityCartDo);

    CommodityCartDo selectById(Long id);

    Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize);

    void deleteByUserId(Long userId);
}
