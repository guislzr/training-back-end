package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.po.OrderItemPo;
import com.fc.common.vo.OrderCreateVo;
import com.fc.common.vo.OrderItemCreateVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 13:40
 */
public interface OrderItemService extends IService<OrderItemPo> {
    /**
     * 创建新订单项
     * @param orderItemCreateVo 订单项
     */
    Long create(Long userId,OrderItemCreateVo orderItemCreateVo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param orderItemDo 订单do
     */
    void update(OrderItemDo orderItemDo);

    OrderItemDo selectById(Long id);

    List<OrderItemDo> getByIds(List<Long> ids);
}
