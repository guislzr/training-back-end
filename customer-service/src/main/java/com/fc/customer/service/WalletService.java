package com.fc.customer.service;

import com.fc.common.domain.OrderDo;
import com.fc.common.domain.WalletDo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:25
 */

public interface WalletService {
    /**
     * 创建新通知
     * @param walletDo 钱包do
     */
    void create(WalletDo walletDo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param walletDo 钱包do
     */
    void update(WalletDo walletDo);

    WalletDo selectById(Long id);
}
