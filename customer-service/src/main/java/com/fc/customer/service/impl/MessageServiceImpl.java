package com.fc.customer.service.impl;

import com.fc.common.convert.HomeBannerDataConvert;
import com.fc.common.convert.MessageDataConvert;
import com.fc.common.domain.HomeBannerDo;
import com.fc.common.domain.MessageDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.HomeBannerMapper;
import com.fc.common.mapper.MessageMapper;
import com.fc.common.po.HomeBannerPo;
import com.fc.common.po.MessagePo;
import com.fc.customer.service.MessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 11:29
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Resource
    private MessageMapper messageMapper;

    @Resource
    private MessageDataConvert messageDataConvert;

    @Override
    public void create(MessageDo messageDo) {
        MessagePo convertPo = messageDataConvert.convert(messageDo);
        messageMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = messageMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(MessageDo messageDo) {
        MessagePo convert = messageDataConvert.convert(messageDo);
        convert.setId(messageDo.getId());
        int update = messageMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public MessageDo selectById(Long id) {
        MessagePo messagePo = messageMapper.selectById(id);
        return messageDataConvert.convert(messagePo);
    }
}
