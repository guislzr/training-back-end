package com.fc.customer.service;

import java.util.Map;

/**
 * @author lzr
 * @date 2021/3/29 21:56
 */
public interface CustomerInfoService {

    /**
     * 显示用户消息列表
     *
     * @param userId
     * @return
     */
    Map<String, Object> showMessageList(Long userId);
}
