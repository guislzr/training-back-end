package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.HomeBannerDataConvert;
import com.fc.common.domain.HomeBannerDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.HomeBannerMapper;
import com.fc.common.po.HomeBannerPo;
import com.fc.common.po.ReportPo;
import com.fc.customer.service.HomeBannerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:33
 */
@Service
public class HomeBannerServiceImpl extends ServiceImpl<HomeBannerMapper, HomeBannerPo> implements HomeBannerService {
    @Resource
    private HomeBannerMapper homeBannerMapper;

    @Resource
    private HomeBannerDataConvert homeBannerDataConvert;

    @Override
    public void create(HomeBannerDo homeBannerDo) {

        HomeBannerPo convertPo = homeBannerDataConvert.convert(homeBannerDo);
        homeBannerMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = homeBannerMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(HomeBannerDo homeBannerDo) {
        HomeBannerPo convert = homeBannerDataConvert.convert(homeBannerDo);
        convert.setId(homeBannerDo.getId());
        int update = homeBannerMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public HomeBannerDo selectById(Long id) {
        HomeBannerPo homeBannerPo = homeBannerMapper.selectById(id);
        return homeBannerDataConvert.convert(homeBannerPo);
    }

    @Override
    public Map<String, Object> show(Integer pageNum, Integer pageSize) {

        Page<HomeBannerPo> page = page(new Page<>(pageNum, pageSize));
        List<HomeBannerPo> homeBannerPos = page.getRecords();
        List<HomeBannerDo> homeBannerDos = homeBannerPos.stream().map(homeBannerDataConvert::convert).collect(Collectors.toList());
        return new LinkedHashMap<String, Object>(){
            {
                put("record", homeBannerDos);
                put("current", page.getCurrent());
                put("total", page.getTotal());
            }
        };
        //使用map操作可以遍历集合中的每个对象，并对其进行操作
        //map之后，用.collect(Collectors.toList())会得到操作后的集合。
    }
}
