package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.mapper.SearchRecordMapper;
import com.fc.common.po.SearchRecordPo;
import com.fc.customer.service.SearchRecordService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lzr
 * @date 2021/7/10 14:22
 */
@Service
public class SearchRecordServiceImpl extends ServiceImpl<SearchRecordMapper, SearchRecordPo> implements SearchRecordService {
    @Override
    public Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize) {
        LambdaQueryWrapper<SearchRecordPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SearchRecordPo::getUserId, userId);
        Page<SearchRecordPo> page = page(new Page<>(pageNum, pageSize), lambdaQueryWrapper);
        return new LinkedHashMap<String, Object>(){
            {
                put("current", page.getCurrent());
                put("total", page.getTotal());
                put("records", page.getRecords());
            }
        };
    }
}
