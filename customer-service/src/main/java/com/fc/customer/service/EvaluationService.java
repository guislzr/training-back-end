package com.fc.customer.service;

import com.fc.common.domain.EvaluationDo;
import java.util.*;
/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:20
 */
public interface EvaluationService {
    /**
     * 创建评价
     * @param evaluationDo 评价do
     */
    void create(EvaluationDo evaluationDo);

    /**
     * 通过id删除评价
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新评价
     * @param evaluationDo 评价do
     */
    void update(EvaluationDo evaluationDo);

    EvaluationDo selectById(Long id);

    /**
     * 最新的评论
     */
    List<EvaluationDo> selectByDate(Long commodityId);

}
