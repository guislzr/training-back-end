package com.fc.customer.service;

import com.fc.common.domain.HomeTypeDo;

import java.util.List;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:20
 */
public interface HomeTypeService{
    /**
     * 创建新类型
     * @param homeTypeDo 首页分类按钮的do
     */
    void create(HomeTypeDo homeTypeDo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param homeTypeDo 首页分类按钮do
     */
    void update(HomeTypeDo homeTypeDo);

    HomeTypeDo selectById(Long id);

    /**
     * 查询所有type
     * @return
     */
    List<HomeTypeDo> showAll();

    Boolean saveAll(List<HomeTypeDo> homeTypeDos);
}
