package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.common.convert.OrderDataConvert;
import com.fc.common.convert.UserDataConvert;
import com.fc.common.convert.UserDomainConvert;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.UserDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.OrderMapper;
import com.fc.common.mapper.UserMapper;
import com.fc.common.po.OrderPo;
import com.fc.common.po.ReportPo;
import com.fc.common.po.UserPo;
import com.fc.common.vo.UserCreateVo;
import com.fc.common.vo.UserVo;
import com.fc.customer.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:55
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Resource
    private UserDataConvert userDataConvert;

    @Resource
    private UserDomainConvert userDomainConvert;

    @Override
    public Long create(UserPo userPo) {
        UserPo user = selectByPhone(userPo.getPhone());
        if(Objects.nonNull(user)){
            throw new BusinessException("手机号重复");
        }
        userMapper.insert(userPo);
        return userPo.getId();
    }

    @Override
    public Long login(UserPo userPo) {
       UserPo user = selectByPhone(userPo.getPhone());
        if(Objects.isNull(user)){
            throw new BusinessException("未找到该手机号用户");
        }
        if(userPo.getPassword().equals(user.getPassword())){
            return user.getId();
        }
        return null;

    }



    @Override
    public void deleteById(Long id) {
        int i = userMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(UserPo userPo) {
        int update = userMapper.updateById(userPo);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public UserDo selectById(Long id) {
        UserPo userPo = userMapper.selectById(id);
        return userDataConvert.convert(userPo);
    }


    @Override
    public UserPo selectByPhone(String phone) {
        QueryWrapper<UserPo> wrapper = new QueryWrapper<>();
        wrapper.eq("phone", phone);
        return userMapper.selectOne(wrapper);
    }
}
