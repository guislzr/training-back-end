package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.AddressDataConvert;
import com.fc.common.domain.AddressDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.AddressMapper;
import com.fc.common.po.AddressPo;
import com.fc.customer.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 14:04
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, AddressPo> implements AddressService {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressDataConvert addressDataConvert;

    @Override
    public void create(AddressDo addressDo) {
        AddressPo convertPo = addressDataConvert.convert(addressDo);
        addressMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = addressMapper.deleteById(id);
        if(i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(AddressDo addressDo) {
        AddressPo convert = addressDataConvert.convert(addressDo);
        convert.setId(addressDo.getId());
        updateById(convert);
    }

    @Override
    public AddressDo selectById(Long id) {
        AddressPo addressPo = addressMapper.selectById(id);
        return addressDataConvert.convert(addressPo);
    }

    @Override
    public List<AddressDo> selectByUserId(Long userId) {
        QueryWrapper<AddressPo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<AddressPo> addressPos = addressMapper.selectList(wrapper);
        return addressPos.stream().map(addressPo -> addressDataConvert.convert(addressPo))
                .collect(Collectors.toList());
    }

    @Override
    public AddressDo selectDefaultAddress(Long userId) {
        QueryWrapper<AddressPo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("is_default", true);
        List<AddressPo> addressPos = addressMapper.selectList(wrapper);
        AddressDo ret;
        if(addressPos.isEmpty()){
            throw new BusinessException("未设置默认收货地址");
        } else {
            ret = addressDataConvert.convert(addressPos.get(0));
        }
        return ret;
    }
}
