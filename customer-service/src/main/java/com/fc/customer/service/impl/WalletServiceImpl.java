package com.fc.customer.service.impl;

import com.fc.common.convert.OrderDataConvert;
import com.fc.common.convert.WalletDataConvert;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.WalletDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.OrderMapper;
import com.fc.common.mapper.WalletMapper;
import com.fc.common.po.OrderPo;
import com.fc.common.po.WalletPo;
import com.fc.customer.service.WalletService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:26
 */
@Service
public class WalletServiceImpl implements WalletService {
    @Resource
    private WalletMapper walletMapper;

    @Resource
    private WalletDataConvert walletDataConvert;

    @Override
    public void create(WalletDo walletDo) {
        WalletPo convertPo = walletDataConvert.convert(walletDo);
        walletMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = walletMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(WalletDo walletDo) {
        WalletPo convert = walletDataConvert.convert(walletDo);
        convert.setId(walletDo.getId());
        int update = walletMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public WalletDo selectById(Long id) {
        WalletPo walletPo = walletMapper.selectById(id);
        return walletDataConvert.convert(walletPo);
    }
}
