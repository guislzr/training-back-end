package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.*;
import com.fc.common.domain.HomeTypeDo;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.*;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.HomeTypePo;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.OrderStatus;
import com.fc.common.vo.*;
import com.fc.customer.service.DiscountService;
import com.fc.customer.service.OrderItemService;
import com.fc.customer.service.OrderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 13:07
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderPo> implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    private OrderDataConvert orderDataConvert;

    @Resource
    private OrderDomainConvert orderDomainConvert;

    @Resource
    private BOrderDomainConvert bOrderDomainConvert;

    @Resource
    private OrderItemService orderItemService;


    @Resource
    private OrderItemMapper orderItemMapper;

    @Resource
    private OrderItemDataConvert orderItemDataConvert;

    @Resource
    private OrderItemDomainConvert orderItemDomainConvert;

    @Resource
    private CommodityMapper commodityMapper;


    /**
     * 创建订单：
     * 形成未支付订单（这个时候之前需要给前端发送优惠券相关的，便于用户选择可用优惠券）
     * 把属于同一个店铺的所有订单项形成一个订单，计算其金额等属性。
     * 支付操作后
     * 进入物流或者终止订单
     *
     * @param orderCreateVo      订单vo
     * @param orderItemCreateVos 订单项列表
     */
    @Override
    public void create(OrderCreateVo orderCreateVo, List<OrderItemCreateVo> orderItemCreateVos) {
        OrderDo orderDo = orderDomainConvert.convert(orderCreateVo);
        /**
         * 计算总优惠金额
         */
        Double totalPayment = 0.0;
        for (OrderItemCreateVo orderItem : orderItemCreateVos) {
            totalPayment += orderItem.getDiscount();
        }
        orderDo.setDiscount(totalPayment);
        orderDo.setStatus(OrderStatus.UNPAY);
        /**
         * 存订单项
         */
        List<Long> itemIds = new ArrayList<>();
        for (OrderItemCreateVo orderItem : orderItemCreateVos) {
            Long id = orderItemService.create(orderCreateVo.getUserId(), orderItem);
            itemIds.add(id);
        }
        orderDo.setOrderItemIds(itemIds);
        OrderPo convertPo = orderDataConvert.convert(orderDo);
        orderMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = orderMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(OrderDo orderDo) {
        OrderPo convert = orderDataConvert.convert(orderDo);
        convert.setId(orderDo.getId());
        int update = orderMapper.update(convert, null);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public OrderDo selectById(Long id) {
        OrderPo orderPo = orderMapper.selectById(id);
        return orderDataConvert.convert(orderPo);
    }

    @Override
    public Map<String, Object> selectByStoreId(Long storeId, OrderStatus orderStatus, Integer page, Integer size) {
        QueryWrapper<OrderPo> orderWrapper = new QueryWrapper<>();
        if (Objects.isNull(orderStatus)) {
            orderWrapper.eq("store_id", storeId);
        } else {
            orderWrapper.eq("store_id", storeId).eq("status", orderStatus);
        }
        orderWrapper.orderByDesc("gmt_create");
        Page<OrderPo> orderPoPage = new Page<>(page - 1, size);
        IPage<OrderPo> orderPoIPage = orderMapper.selectPage(orderPoPage, orderWrapper);

        List<OrderDo> doRecords = orderPoIPage.getRecords().stream().map(orderPo -> orderDataConvert.convert(orderPo))
                .collect(Collectors.toList());
        List<BOrderShowVo> records = new ArrayList<>();
        for (OrderDo orderDo : doRecords) {
            QueryWrapper<OrderPo> orderItemWrapper = new QueryWrapper<>();
            orderItemWrapper.eq("id", orderDo.getId());
            List<OrderItemShowVo> orderItemShowVos = new ArrayList<>();
            for (long i : orderDo.getOrderItemIds()) {
                OrderItemShowVo orderItemShowVo = orderItemDomainConvert.convert(orderItemDataConvert.convert(orderItemMapper.selectById(i)));
                orderItemShowVo.setPicture(commodityMapper.selectById(orderItemShowVo.getCommodityId()).getSubImages());
                orderItemShowVos.add(orderItemShowVo);
            }
            BOrderShowVo vo = bOrderDomainConvert.convert(orderDo);
            vo.setOrderItems(orderItemShowVos);
            records.add(vo);
        }
        return new HashMap<String, Object>() {
            {
                put("records", records);
                put("totalPages", orderPoIPage.getPages());
                put("current", orderPoIPage.getCurrent());
            }
        };
    }

    @Override
    public void updatePaidOrder(Long orderId) {
        UpdateWrapper<OrderPo> wrapper = new UpdateWrapper<>();
        wrapper.eq("id", orderId).set("status", OrderStatus.HAVEBUY);
        orderMapper.update(null, wrapper);
    }

    @Override
    public Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize) {
        LambdaQueryWrapper<OrderPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(OrderPo::getUserId, userId);
        Page<OrderPo> page = page(new Page<>(pageNum, pageSize), lambdaQueryWrapper);
        List<OrderPo> orderPos = page.getRecords();
        List<OrderDo> orderDos = orderPos.stream().map(orderDataConvert::convert).collect(Collectors.toList());
        return new LinkedHashMap<String, Object>(){
            {
                put("records", orderDos);
                put("current", page.getCurrent());
                put("total", page.getTotal());
            }
        };
    }
}
