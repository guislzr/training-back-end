package com.fc.customer.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.CommodityDataConvert;
import com.fc.common.convert.CommodityDomainConvert;
import com.fc.common.core.ProductDTO;
import com.fc.common.core.RelateDTO;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.StoreMapper;
import com.fc.common.po.*;
import com.fc.common.vo.CommodityVo;
import com.fc.customer.service.CommodityListService;
import com.fc.customer.service.OrderItemService;
import com.fc.customer.service.OrderService;
import com.fc.customer.service.SearchRecordService;
import com.fc.customer.vo.SortVo;
import org.apache.commons.lang3.StringUtils;
import org.javatuples.Triplet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lzr
 * @date 2021/3/23 15:56
 */
@Service
public class CommodityListServiceImpl extends ServiceImpl<CommodityMapper, CommodityPo> implements CommodityListService {

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private SearchRecordService searchRecordService;

    @Autowired
    private CommodityMapper commodityMapper;

    @Autowired
    private CommodityDataConvert commodityDataConvert;

    @Autowired
    private CommodityDomainConvert commodityDomainConvert;

    @Autowired
    private OrderService orderService;

    @Autowired
    private StoreMapper storeMapper;

    /**
     * 展示商品列表
     *
     * @param page 页数
     * @param size 每页多少个
     * @return
     */
    @Override
    public Map<String, Object> showCommodityList(Integer page, Integer size) {
        QueryWrapper<CommodityPo> commodityWrapper = new QueryWrapper<>();
        commodityWrapper.orderByDesc("gmt_create");
        Page<CommodityPo> commodityPoPage = new Page<>(page, size);
        IPage<CommodityPo> commodityPoIPage = commodityMapper.selectPage(commodityPoPage, commodityWrapper);

        List<CommodityVo> records = commodityPoIPage.getRecords().stream().map(commodityPo -> commodityDataConvert.convert(commodityPo))
                .map(commodityDo -> commodityDomainConvert.convert(commodityDo))
                .collect(Collectors.toList());

        return new HashMap<String, Object>() {
            {
                put("records", records);
                put("totalPages", commodityPoIPage.getPages());
                put("current", commodityPoIPage.getCurrent());
            }
        };
    }

    @Override
    public CommodityVo getCommodityDetail(Long id) {
        CommodityPo commodityPo = commodityMapper.selectById(id);
        CommodityVo commodityVo = commodityDomainConvert.convert(commodityDataConvert.convert(commodityPo));
        StorePo storePo = storeMapper.selectById(commodityPo.getStoreId());
        commodityVo.setStoreName(storePo.getStoreName());
        return commodityVo;
    }

    @Override
    public Map<String, Object> searchCommodityList(String type, Integer page, Integer size, SortVo sortVo) {
        LambdaQueryWrapper<CommodityPo> commodityWrapper = new LambdaQueryWrapper<>();
        commodityWrapper.like(CommodityPo::getType, type);
        if (StringUtils.isNotBlank(sortVo.getType())) {
            if ("price".equals(sortVo.getType())) {
                if ("desc".equals(sortVo.getSort())) {
                    commodityWrapper.orderByDesc(CommodityPo::getRentPrice);
                } else {
                    commodityWrapper.orderByAsc(CommodityPo::getRentPrice);
                }
            } else {
                if ("desc".equals(sortVo.getSort())) {
                    commodityWrapper.orderByDesc(CommodityPo::getGmtCreate);
                } else {
                    commodityWrapper.orderByAsc(CommodityPo::getGmtCreate);
                }
            }
        }
        Page<CommodityPo> commodityPoPage = new Page<>(page, size);
        IPage<CommodityPo> commodityPoIPage = commodityMapper.selectPage(commodityPoPage, commodityWrapper);

        List<CommodityVo> records = commodityPoIPage.getRecords().stream().map(commodityPo -> commodityDataConvert.convert(commodityPo))
                .map(commodityDo -> commodityDomainConvert.convert(commodityDo))
                .collect(Collectors.toList());

        return new HashMap<String, Object>() {
            {
                put("records", records);
                put("total", commodityPoIPage.getTotal());
                put("current", commodityPoIPage.getCurrent());
            }
        };
    }

    @Override
    public List<CommodityDo> showHot() {
        List<OrderItemPo> orderItemPos = orderItemService.list();
        Map<Long, List<OrderItemPo>> collect = orderItemPos.stream().collect(Collectors.groupingBy(OrderItemPo::getCommodityId, Collectors.toList()));
        List<Triplet<Long, Integer, Long>> list = new LinkedList<>();
        collect.forEach((k, v) -> {
            list.add(new Triplet<>(k, v.size(), null));
        });
        List<Long> ids = list.stream().sorted(Comparator.comparing(Triplet::getValue1)).sorted(Comparator.reverseOrder())
                .map(Triplet::getValue0).collect(Collectors.toList());
        return commodityMapper.selectBatchIds(ids).stream().map(commodityDataConvert::convert).collect(Collectors.toList());
    }

    @Override
    public List<CommodityDo> getRecord(Long userId) {
        List<SearchRecordPo> searchRecordPos = searchRecordService.lambdaQuery().eq(SearchRecordPo::getUserId, userId).list();
        List<Long> ids = searchRecordPos.stream().map(SearchRecordPo::getCommodityId).collect(Collectors.toList());
        List<CommodityPo> commodityPos = commodityMapper.selectBatchIds(ids);
        return commodityPos.stream().map(commodityDataConvert::convert).collect(Collectors.toList());
    }

    @Override
    public Map<String, Object> searchByName(String name, Integer pageNum, Integer pageSize, SortVo sortVo) {
        LambdaQueryWrapper<CommodityPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(CommodityPo::getCommodityName, name);
        if (StringUtils.isNotBlank(sortVo.getType())) {
            if ("price".equals(sortVo.getType())) {
                if ("desc".equals(sortVo.getSort())) {
                    lambdaQueryWrapper.orderByDesc(CommodityPo::getRentPrice);
                } else {
                    lambdaQueryWrapper.orderByAsc(CommodityPo::getRentPrice);
                }
            } else {
                if ("desc".equals(sortVo.getSort())) {
                    lambdaQueryWrapper.orderByDesc(CommodityPo::getGmtCreate);
                } else {
                    lambdaQueryWrapper.orderByAsc(CommodityPo::getGmtCreate);
                }
            }
        }
        Page<CommodityPo> page = page(new Page<>(pageNum, pageSize), lambdaQueryWrapper);
        List<CommodityPo> commodityPos = page.getRecords();
        List<CommodityVo> records = commodityPos.stream()
                .map(commodityDataConvert::convert).map(commodityDomainConvert::convert).collect(Collectors.toList());
        return new LinkedHashMap<String, Object>() {
            {
                put("records", records);
                put("current", page.getCurrent());
                put("total", page.getTotal());
            }
        };
    }

    @Override
    public List<CommodityDo> getRecommend(Long userId) {
        List<CommodityDo> recommendGoods;
        //协同过滤算法
//        CoreMath coreMath = new CoreMath();
        //获取商品数据
//        List<RelateDTO> relateDTOList = getRelate();
        //执行算法，返回推荐商品id
//        List<Integer> recommendIdLists = coreMath.recommend(userId.intValue(), relateDTOList);
//        if (null == recommendIdLists || recommendIdLists.isEmpty()) {
//            recommendGoods = null;
//        } else {
//            //获取商品DTO（这里的过滤是防止商品表该id商品已下架或删除）
//            List<ProductDTO> productDTOList = getCommodity().stream().filter(e -> recommendIdLists.contains(e.getProductId())).collect(Collectors.toList());
//            //获取商品ids
//            List<Integer> goodIds = productDTOList.stream().map(e -> e.getProductId()).collect(Collectors.toList());
//            List<Long> goodIds2 = JSONArray.parseArray(goodIds.toString(), Long.class);
//            List<CommodityPo> commodityPos = listByIds(goodIds2);
//            recommendGoods = commodityPos.stream().map(commodityDataConvert::convert).collect(Collectors.toList());
//        }
        List<CommodityPo> commodityPos = commodityMapper.selectList(null);
        Random random = new Random();
        int first = random.nextInt(20) + 20;
        commodityPos = commodityPos.subList(first, first+10);
        return commodityPos.stream().map(commodityDataConvert::convert).collect(Collectors.toList());
    }

    private List<ProductDTO> getCommodity() {
        List<ProductDTO> productDTOList = new ArrayList<>();
        List<CommodityPo> goodsList = list();
        List<Long> goodIds = goodsList.stream().map(CommodityPo::getId).collect(Collectors.toList());
        for (Long goodId : goodIds) {
            ProductDTO productDTO = new ProductDTO();
            productDTO.setProductId(goodId.intValue());
            productDTOList.add(productDTO);
        }
        return productDTOList;
    }

    public List<RelateDTO> getRelate() {
        List<RelateDTO> relateDTOList = new ArrayList<>();
        List<OrderPo> orders = orderService.list();
        for (OrderPo order : orders) {
            List<OrderItemDo> orderItems = orderItemService.getByIds(JSONObject.parseArray(order.getOrderItemIds(), Long.class));
            for (OrderItemDo item : orderItems) {
                RelateDTO relateDTO = new RelateDTO();
                relateDTO.setUserId(order.getUserId().intValue());
                relateDTO.setProductId(item.getCommodityId().intValue());
                //模拟购买商品的次数(点击浏览商品的次数）
                relateDTO.setIndex((int) (Math.random() * 10 + 1));
                //不能设置为1，否则皮尔森系数计算为0.0
                //relateDTO.setIndex(1);
                relateDTOList.add(relateDTO);
            }

        }
        return relateDTOList;
    }

}
