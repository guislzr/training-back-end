package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.FavoriteDataConvert;
import com.fc.common.convert.FavoriteDomainConvert;
import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.FavoriteDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.FavoriteMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.FavoritePo;
import com.fc.common.vo.FavoriteVo;
import com.fc.customer.service.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 13:12
 */
@Service
public class FavoriteServiceImpl extends ServiceImpl<FavoriteMapper, FavoritePo> implements FavoriteService {

    @Autowired
    private FavoriteMapper favoriteMapper;

    @Autowired
    private CommodityMapper commodityMapper;

    @Autowired
    private FavoriteDataConvert favoriteDataConvert;

    @Autowired
    private FavoriteDomainConvert favoriteDomainConvert;

    @Override
    public Boolean create(FavoriteDo favoriteDo) {
        FavoritePo one = lambdaQuery().eq(FavoritePo::getUserId, favoriteDo.getUserId()).eq(FavoritePo::getCommodityId, favoriteDo.getCommodityId()).one();
        if(Objects.nonNull(one)){
            return false;
        }
        FavoritePo convertPo = favoriteDataConvert.convert(favoriteDo);
        return save(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = favoriteMapper.deleteById(id);
        if(i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(FavoriteDo favoriteDo) {
        FavoritePo convert = favoriteDataConvert.convert(favoriteDo);
        convert.setId(favoriteDo.getId());
        int update = favoriteMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public FavoriteDo selectById(Long id) {
        FavoritePo favoritePo = favoriteMapper.selectById(id);
        return favoriteDataConvert.convert(favoritePo);
    }

    @Override
    public List<FavoriteVo> selectByUserId(Long id) {
        QueryWrapper<FavoritePo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", id);
        List<FavoritePo> favoritePos = favoriteMapper.selectList(wrapper);
        List<FavoriteDo> favoriteDos = favoritePos.stream().map(favoritePo -> favoriteDataConvert.convert(favoritePo))
                .collect(Collectors.toList());
        List<FavoriteVo> favoriteVos = favoriteDos.stream().map(favoriteDo -> favoriteDomainConvert.convert(favoriteDo))
                .collect(Collectors.toList());
        for (FavoriteVo vo :
                favoriteVos) {
            CommodityPo commodityPo = commodityMapper.selectById(vo.getCommodityId());
            vo.setCommodityName(commodityPo.getCommodityName());
            vo.setDescription(commodityPo.getDescription());
            vo.setSubImages(commodityPo.getSubImages());
            QueryWrapper<FavoritePo> favoritePoQueryWrapper = new QueryWrapper<>();
            favoritePoQueryWrapper.eq("commodity_id", vo.getCommodityId());
            vo.setStarCount(favoriteMapper.selectCount(favoritePoQueryWrapper));
        }
        return favoriteVos;
    }
}
