package com.fc.customer.service;

import com.fc.common.domain.CommodityDo;
import com.fc.common.vo.CommodityVo;
import com.fc.customer.vo.SortVo;

import java.util.*;
/**
 * @author lzr
 * @date 2021/3/23 15:55
 */
public interface CommodityListService {

    /**
     * 首页用户商品列表
     * @param page
     * @param size
     * @return
     */
    Map<String, Object> showCommodityList(Integer page, Integer size);

    /**
     * 获取商品详情页
     *
     * @param id 商品id
     * @return
     */
    CommodityVo getCommodityDetail(Long id);

    /**
     * 搜索商品
     *
     * @param type 商品类型
     * @param page 页码
     * @param size 大小
     * @return
     */
    Map<String, Object> searchCommodityList(String type, Integer page, Integer size, SortVo sortVo);

    List<CommodityDo> showHot();

    List<CommodityDo> getRecord(Long userId);

    Map<String, Object> searchByName(String name, Integer pageNum, Integer pageSize, SortVo sortVo);

    List<CommodityDo> getRecommend(Long userId);

}
