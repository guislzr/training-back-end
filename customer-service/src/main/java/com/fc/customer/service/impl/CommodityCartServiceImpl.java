package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.CommodityCartDataConvert;
import com.fc.common.convert.CommodityCartDomainConvert;
import com.fc.common.domain.CommodityCartDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.CommodityCartMapper;
import com.fc.common.po.CommodityCartPo;
import com.fc.common.vo.CommodityCartVo;
import com.fc.customer.service.CommodityCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 12:08
 */
@Service
public class CommodityCartServiceImpl extends ServiceImpl<CommodityCartMapper, CommodityCartPo> implements CommodityCartService {
    @Resource
    private CommodityCartMapper commodityCartMapper;

    @Resource
    private CommodityCartDataConvert commodityCartDataConvert;

    @Autowired
    private CommodityCartDomainConvert commodityCartDomainConvert;

    @Override
    public void create(CommodityCartDo commodityCartDo) {
        CommodityCartPo commodityCartPo = commodityCartDataConvert.convert(commodityCartDo);
        commodityCartMapper.insert(commodityCartPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = commodityCartMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(CommodityCartDo commodityCartDo) {
        CommodityCartPo convert = commodityCartDataConvert.convert(commodityCartDo);
        convert.setId(commodityCartDo.getId());
        updateById(convert);
    }
    @Override
    public CommodityCartDo selectById(Long id) {
        CommodityCartPo commodityCartPo = commodityCartMapper.selectById(id);
        return commodityCartDataConvert.convert(commodityCartPo);
    }

    @Override
    public Map<String, Object> getByUserId(Long userId, Integer pageNum, Integer pageSize) {
        LambdaQueryWrapper<CommodityCartPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(CommodityCartPo::getUserId, userId);
        Page<CommodityCartPo> page = page(new Page<>(pageNum, pageSize), lambdaQueryWrapper);
        List<CommodityCartPo> commodityCartPos = page.getRecords();
        List<CommodityCartVo> records = commodityCartPos.stream()
                .map(commodityCartDataConvert::convert).map(commodityCartDomainConvert::convert).collect(Collectors.toList());
        return new LinkedHashMap<String, Object>(){
            {
                put("records", records);
                put("current", page.getCurrent());
                put("total", page.getTotal());
            }
        };
    }

    @Override
    public void deleteByUserId(Long userId) {
        LambdaQueryWrapper<CommodityCartPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(CommodityCartPo::getUserId, userId);
        remove(lambdaQueryWrapper);
    }

}
