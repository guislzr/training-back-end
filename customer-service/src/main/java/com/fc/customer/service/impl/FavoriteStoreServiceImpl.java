package com.fc.customer.service.impl;

import ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.FavoriteStoreDataConvert;
import com.fc.common.convert.FavoriteStoreDomainConvert;
import com.fc.common.domain.FavoriteStoreDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.FavoriteStoreMapper;
import com.fc.common.mapper.StoreMapper;
import com.fc.common.po.CommodityPo;
import com.fc.common.po.FavoriteStorePo;
import com.fc.common.po.StorePo;
import com.fc.common.vo.FavoriteStoreVo;
import com.fc.customer.service.FavoriteStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:19
 */
@Service
public class FavoriteStoreServiceImpl extends ServiceImpl<FavoriteStoreMapper, FavoriteStorePo> implements FavoriteStoreService {

    @Autowired
    private FavoriteStoreMapper favoriteStoreMapper;

    @Autowired
    private FavoriteStoreDataConvert favoriteStoreDataConvert;

    @Autowired
    private FavoriteStoreDomainConvert favoriteStoreDomainConvert;

    @Autowired
    private StoreMapper storeMapper;

    @Autowired
    private CommodityMapper commodityMapper;

    @Override
    public Boolean create(FavoriteStoreDo favoriteStoreDo) {
        FavoriteStorePo one = lambdaQuery().eq(FavoriteStorePo::getUserId, favoriteStoreDo.getUserId()).eq(FavoriteStorePo::getStoreId, favoriteStoreDo.getStoreId()).one();
        if(Objects.nonNull(one)){
            return false;
        }
        FavoriteStorePo convertPo = favoriteStoreDataConvert.convert(favoriteStoreDo);
        return save(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = favoriteStoreMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(FavoriteStoreDo favoriteStoreDo) {
        FavoriteStorePo convert = favoriteStoreDataConvert.convert(favoriteStoreDo);
        convert.setId(favoriteStoreDo.getId());
        int update = favoriteStoreMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public FavoriteStoreDo selectById(Long id) {
        FavoriteStorePo favoriteStorePo = favoriteStoreMapper.selectById(id);
        return favoriteStoreDataConvert.convert(favoriteStorePo);
    }

    @Override
    public List<FavoriteStoreVo> selectByUserId(Long id) throws ParseException {
        QueryWrapper<FavoriteStorePo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", id);
        List<FavoriteStorePo> favoriteStorePos = favoriteStoreMapper.selectList(wrapper);
        List<FavoriteStoreDo> favoriteStoreDos = favoriteStorePos.stream().map(favoriteStorePo -> favoriteStoreDataConvert.convert(favoriteStorePo))
                                                    .collect(Collectors.toList());
        List<FavoriteStoreVo> favoriteStoreVos = favoriteStoreDos.stream().map(favoriteStoreDo -> favoriteStoreDomainConvert.convert(favoriteStoreDo))
                                                    .collect(Collectors.toList());
        for (FavoriteStoreVo vo :
                favoriteStoreVos) {
            StorePo store = storeMapper.selectById(vo.getStoreId());
            vo.setStoreName(store.getStoreName());
            vo.setScore(store.getScore());
            vo.setStoreSubImage(store.getIcon());

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            long startDateTime = dateFormat.parse(dateFormat.format(vo.getSubscribeDate())).getTime();
            long endDateTime = dateFormat.parse(dateFormat.format(new Date())).getTime();
            vo.setSubscribeDate(new Date(((endDateTime - startDateTime) / (1000 * 3600 * 24))));

            QueryWrapper<CommodityPo> commodityPoQueryWrapper = new QueryWrapper<>();
            commodityPoQueryWrapper.eq("store_id", store.getId());
            List<CommodityPo> commodityPos = commodityMapper.selectList(commodityPoQueryWrapper);
            List<String> subImageLists = new ArrayList<>();
            if(commodityPos.size() > 4){
                for(int i = 0; i<4; i++){
                    subImageLists.add(commodityPos.get(i).getSubImages());
                }
            } else{
                for (CommodityPo cp :
                        commodityPos) {
                    subImageLists.add(cp.getSubImages());
                }
            }

            vo.setSubImageLists(subImageLists);
        }
        return favoriteStoreVos;
    }
}
