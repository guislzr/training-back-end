package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fc.common.convert.EvaluationDataConvert;
import com.fc.common.domain.EvaluationDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.CommodityMapper;
import com.fc.common.mapper.EvaluationMapper;
import com.fc.common.po.EvaluationPo;
import com.fc.customer.service.EvaluationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:33
 */
@Service
public class EvaluationServiceImpl implements EvaluationService {
    @Resource
    private EvaluationMapper evaluationMapper;

    @Resource
    private EvaluationDataConvert evaluationDataConvert;

    @Autowired
    private CommodityMapper commodityMapper;

    @Override
    public void create(EvaluationDo evaluationDo) {
        EvaluationPo convertPo = evaluationDataConvert.convert(evaluationDo);
        evaluationMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = evaluationMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(EvaluationDo evaluationDo) {
        EvaluationPo convert = evaluationDataConvert.convert(evaluationDo);
        convert.setId(evaluationDo.getId());
        int update = evaluationMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public EvaluationDo selectById(Long id) {
        EvaluationPo evaluationPo = evaluationMapper.selectById(id);
        return evaluationDataConvert.convert(evaluationPo);
    }

    @Override
    public List<EvaluationDo> selectByDate(Long commodityId){
        LambdaQueryWrapper<EvaluationPo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EvaluationPo::getCommodityId, commodityId);
        lambdaQueryWrapper.orderByDesc(EvaluationPo::getGmtCreate);
        return evaluationMapper.selectList(lambdaQueryWrapper).stream()
                .map(evaluationDataConvert::convert).collect(Collectors.toList());
    }

}
