package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.AddressDo;
import com.fc.common.po.AddressPo;

import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/24 13:56
 */
public interface AddressService extends IService<AddressPo> {

    /**
     * 创建一个用户收货地址
     * @param addressDo
     */
    void create(AddressDo addressDo);

    /**
     * 根据id删除一个收货地址
     * @param id
     */
    void deleteById(Long id);

    /**
     * 更新收货地址
     * @param addressDo
     */
    void update(AddressDo addressDo);

    /**
     * 根据id获取一个收货地址
     * @param id
     * @return
     */
    AddressDo selectById(Long id);

    /**
     * 根据用户id获取其收获地址
     * @param userId
     * @return
     */
    List<AddressDo> selectByUserId(Long userId);

    /**
     * 获取用户默认收货地址
     * @return
     */
    AddressDo selectDefaultAddress(Long userId);
}
