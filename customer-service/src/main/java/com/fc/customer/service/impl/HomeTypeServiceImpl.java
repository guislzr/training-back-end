package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.HomeTypeDataConvert;
import com.fc.common.domain.HomeTypeDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.HomeTypeMapper;
import com.fc.common.po.HomeTypePo;
import com.fc.customer.service.HomeTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:33
 */
@Service
public class HomeTypeServiceImpl extends ServiceImpl<HomeTypeMapper, HomeTypePo> implements HomeTypeService {
    @Resource
    private HomeTypeMapper homeTypeMapper;

    @Resource
    private HomeTypeDataConvert homeTypeDataConvert;

    @Override
    public void create(HomeTypeDo homeTypeDo) {
        HomeTypePo convertPo = homeTypeDataConvert.convert(homeTypeDo);
        homeTypeMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = homeTypeMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(HomeTypeDo homeTypeDo) {
        HomeTypePo convert = homeTypeDataConvert.convert(homeTypeDo);
        convert.setId(homeTypeDo.getId());
        int update = homeTypeMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public HomeTypeDo selectById(Long id) {
        HomeTypePo homeTypePo = homeTypeMapper.selectById(id);
        return homeTypeDataConvert.convert(homeTypePo);
    }

    @Override
    public List<HomeTypeDo> showAll() {
        /**
         * TODO 用户id自定义推荐
         */
        QueryWrapper<HomeTypePo> wrapper = new QueryWrapper<>();
        List<HomeTypePo> homeTypePoList = homeTypeMapper.selectList(wrapper);
        return homeTypePoList.stream().map(homeTypePo -> homeTypeDataConvert.convert(homeTypePo))
                .collect(Collectors.toList());
        //使用map操作可以遍历集合中的每个对象，并对其进行操作
        //map之后，用.collect(Collectors.toList())会得到操作后的集合。

    }

    @Override
    public Boolean saveAll(List<HomeTypeDo> homeTypeDos) {
        List<HomeTypePo> homeTypePos = homeTypeDos.stream().map(homeTypeDataConvert::convert).collect(Collectors.toList());
        return saveBatch(homeTypePos);
    }
}
