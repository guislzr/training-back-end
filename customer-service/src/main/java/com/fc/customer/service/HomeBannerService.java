package com.fc.customer.service;

import com.fc.common.domain.CommodityDo;
import com.fc.common.domain.HomeBannerDo;

import java.util.List;
import java.util.Map;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 10:19
 */
public interface HomeBannerService {
    /**
     * 创建首页显示图
     * @param homeBannerDo 首页显示图do
     */
    void create(HomeBannerDo homeBannerDo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新首页显示图
     * @param homeBannerDo 首页显示图do
     */
    void update(HomeBannerDo homeBannerDo);

    HomeBannerDo selectById(Long id);

    /**
     * 展示拥有的banner
     * @return
     */
    Map<String, Object> show(Integer pageNum, Integer pageSize);

}
