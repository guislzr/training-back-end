package com.fc.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fc.common.domain.FavoriteStoreDo;
import com.fc.common.po.FavoriteStorePo;
import com.fc.common.vo.FavoriteStoreVo;

import java.text.ParseException;
import java.util.List;

/**
 * @auther: 李梓睿
 * @date: 2021/3/30 14:13
 */
public interface FavoriteStoreService extends IService<FavoriteStorePo> {

    /**
     * 添加关注店铺
     * @param favoriteStoreDo
     */
    Boolean create(FavoriteStoreDo favoriteStoreDo);

    /**
     * 取消对店铺的关注
     * @param id
     */
    void deleteById(Long id);

    /**
     * 更新关注店铺
     * @param favoriteStoreDo
     */
    void update(FavoriteStoreDo favoriteStoreDo);

    FavoriteStoreDo selectById(Long id);

    /**
     * 根据userId查询其关注的店铺
     * @param id
     * @return
     */
    List<FavoriteStoreVo> selectByUserId(Long id) throws ParseException;
}
