package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fc.common.convert.DiscountDataConvert;
import com.fc.common.domain.DiscountDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.DiscountMapper;
import com.fc.common.po.DiscountPo;
import com.fc.customer.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @auther: 李梓睿
 * @date: 2021/3/22 22:53
 */
@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private DiscountMapper discountMapper;

    @Autowired
    private DiscountDataConvert discountDataConvert;

    @Override
    public void create(DiscountDo discountDo) {
        DiscountPo convertPo = discountDataConvert.convert(discountDo);
        discountMapper.insert(convertPo);
    }

    @Override
    public void deleteById(Long id) {
        int i = discountMapper.deleteById(id);
        if(i <= 0){
            throw new BusinessException("删除失败");
        }
    }

    @Override
    public void update(DiscountDo discountDo) {
        DiscountPo convert = discountDataConvert.convert(discountDo);
        convert.setId(discountDo.getId());
        int update = discountMapper.updateById(convert);
        if(update <= 0){
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public DiscountDo selectById(Long id) {
        DiscountPo discountPo = discountMapper.selectById(id);
        return discountDataConvert.convert(discountPo);
    }

    @Override
    public List<DiscountDo> selectAvailableDiscount(Long userId, Long storeId, Double amount) {
        Date date = new Date();
        SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd");

        QueryWrapper<DiscountPo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId)
                .eq("store_id", storeId)
                .le("threshold_price", amount)
                .le("start_date", dateFormat.format(date))
                .ge("expiry_date", dateFormat.format(date));
        List<DiscountPo> discountPos = discountMapper.selectList(wrapper);
        return discountPos.stream().map(discountPo -> discountDataConvert.convert(discountPo)).sorted().collect(Collectors.toList());
    }

    @Override
    public List<DiscountDo> selectByUserId(Long userId) {
        QueryWrapper<DiscountPo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        List<DiscountPo> discountPos = discountMapper.selectList(wrapper);
        return discountPos.stream().map(discountPo -> discountDataConvert.convert(discountPo))
                .collect(Collectors.toList());
    }

    @Override
    public List<DiscountDo> selectByStoreId(Long storeId) {
        QueryWrapper<DiscountPo> wrapper = new QueryWrapper<>();
        wrapper.eq("store_id", storeId);
        List<DiscountPo> discountPos = discountMapper.selectList(wrapper);
        return discountPos.stream().map(discountPo -> discountDataConvert.convert(discountPo))
                .collect(Collectors.toList());
    }
}
