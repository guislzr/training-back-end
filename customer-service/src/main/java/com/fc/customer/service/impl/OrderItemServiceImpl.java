package com.fc.customer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fc.common.convert.OrderDataConvert;
import com.fc.common.convert.OrderDomainConvert;
import com.fc.common.convert.OrderItemDataConvert;
import com.fc.common.convert.OrderItemDomainConvert;
import com.fc.common.domain.OrderDo;
import com.fc.common.domain.OrderItemDo;
import com.fc.common.exception.BusinessException;
import com.fc.common.mapper.OrderItemMapper;
import com.fc.common.mapper.OrderMapper;
import com.fc.common.po.OrderItemPo;
import com.fc.common.po.OrderPo;
import com.fc.common.po.status.OrderStatus;
import com.fc.common.vo.OrderCreateVo;
import com.fc.common.vo.OrderItemCreateVo;
import com.fc.customer.service.DiscountService;
import com.fc.customer.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/4/3 13:39
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItemPo> implements OrderItemService {

    @Resource
    private OrderItemMapper orderItemMapper;

    @Resource
    private OrderItemDataConvert orderItemDataConvert;


    @Resource
    private OrderItemDomainConvert orderItemDomainConvert;



    @Override
    public Long create(Long userId, OrderItemCreateVo orderItemCreateVo) {
        OrderItemDo orderItemDo = orderItemDomainConvert.convert(orderItemCreateVo);
        orderItemDo.setStatus(OrderStatus.UNPAY);
        orderItemDo.setUserId(userId);
        OrderItemPo orderItemPo = orderItemDataConvert.convert(orderItemDo);
        orderItemMapper.insert(orderItemPo);
        return  orderItemPo.getId();
    }


    @Override
    public void deleteById(Long id) {
        int i = orderItemMapper.deleteById(id);
        if (i <= 0) {
            throw new BusinessException("删除失败");
        }
    }


    @Override
    public void update(OrderItemDo orderItemDo) {
        OrderItemPo convert = orderItemDataConvert.convert(orderItemDo);
        convert.setId(orderItemDo.getId());
        int update = orderItemMapper.updateById(convert);
        if (update <= 0) {
            throw new BusinessException("更新失败");
        }
    }

    @Override
    public OrderItemDo selectById(Long id) {
        OrderItemPo orderItemPo = orderItemMapper.selectById(id);
        return orderItemDataConvert.convert(orderItemPo);
    }

    @Override
    public List<OrderItemDo> getByIds(List<Long> ids) {
        return orderItemMapper.selectBatchIds(ids).stream()
                .map(orderItemDataConvert::convert).collect(Collectors.toList());
    }
}
