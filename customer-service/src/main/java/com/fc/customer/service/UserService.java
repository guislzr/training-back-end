package com.fc.customer.service;

import com.fc.common.domain.OrderDo;
import com.fc.common.domain.UserDo;
import com.fc.common.po.UserPo;
import com.fc.common.vo.UserCreateVo;
import com.fc.common.vo.UserVo;

/**
 * @author 李梓睿
 * @version 1.0.0
 * @date 2021/3/30 14:52
 */
public interface UserService {
    /**
     * 创建新用户
     * @param userPo 用户do
     */
    Long create(UserPo userPo);


    /**
     * 创建新用户
     * @param  phone 手机号
     * @param  password 密码
     */
    Long login(UserPo userPo);

    /**
     * 通过id删除
     * @param id id
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param userDo 用户do
     */
    void update(UserPo userPo);

    UserDo selectById(Long id);
    /**
     * 根据手机号查询
     * @param phone 用户手机号
     */
    UserPo selectByPhone(String phone);
}
