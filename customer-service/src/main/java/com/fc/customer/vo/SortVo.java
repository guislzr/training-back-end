package com.fc.customer.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lzr
 * @date 2021/7/19 10:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortVo {

    @ApiModelProperty(value = "price价格 time成交量")
    private String type;

    @ApiModelProperty(value = "asc升序 desc降序")
    private String sort;
}
